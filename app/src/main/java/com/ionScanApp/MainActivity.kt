package com.ionScanApp

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.get
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.ionScanApp.databinding.ActivityMainBinding
import com.ionScanApp.ui.BaseActivity
import com.ionScanApp.ui.common.utils.consume
import com.ionScanApp.ui.common.utils.visible
import com.ionScanApp.ui.login.LoginActivity
import com.mikhaellopez.circularimageview.CircularImageView
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var drawerToggle: ActionBarDrawerToggle

    private val viewModel: MainActivityViewModel by viewModel()

    companion object {
        fun newIntent(context: Context): Intent = Intent(context, MainActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        checkUserLoggedIn()

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMainId.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
        }
        drawerToggle = ActionBarDrawerToggle(this, binding.drawerLayout, R.string.open_drawer, R.string.close_drawer)
        drawerToggle.isDrawerIndicatorEnabled = true
        binding.drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        //navController = findNavController(R.id.nav_host_fragment)
        navController = (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController
        appBarConfiguration = AppBarConfiguration(navController.graph, binding.drawerLayout)
        binding.navView.setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)

        setupNavigation()
        setupObservers()
    }

    private fun setupNavigation() {
        binding.navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.stock_to_project   -> binding.drawerLayout.consume { navController.navigate(R.id.nav_stock_to_project) }
                R.id.stock_to_stock     -> binding.drawerLayout.consume { navController.navigate(R.id.nav_stock_to_stock) }
                R.id.project_to_stock   -> binding.drawerLayout.consume { navController.navigate(R.id.nav_project_to_stock) }
                R.id.project_to_project -> binding.drawerLayout.consume { navController.navigate(R.id.nav_project_to_stock) }
                R.id.picking            -> binding.drawerLayout.consume { navController.navigate(R.id.nav_picking) }
            }
            false
        }

        binding.logout.setOnClickListener {
            viewModel.logout()
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.nav_home                  -> setUserImageVisibility()
                R.id.itemsTransactionsFragment -> setScanIconVisibility()
                else                           -> setUserImageScanIconVisibility()

            }
        }
    }

    private fun setUserImageScanIconVisibility() {
        viewModel.userImageVisible.value = false
        viewModel.scanIconVisible.value = false
    }

    private fun setUserImageVisibility() {
        viewModel.userImageVisible.value = true
      //  viewModel.scanIconVisible.value = false
    }

    private fun setScanIconVisibility() {
        viewModel.userImageVisible.value = false
      //  viewModel.scanIconVisible.value = true
    }

    private fun checkUserLoggedIn() {
        if (viewModel.shouldGoToLogin()) {
            startActivity(LoginActivity.newIntent(this))
            overridePendingTransition(0, 0)
            finish()
        }
    }

    private fun handleLoggedIn(loggedIn: Boolean) {
        if (!loggedIn) {
            startActivity(LoginActivity.newIntent(this))
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        when (navController.graph.startDestination) {
            navController.currentDestination?.id -> navigateUp()
            else                                 -> onBackPressed()
        }
        return true
    }

    private fun navigateUp(){
        navController.navigateUp(appBarConfiguration)
    }

    private fun setupObservers() {

        viewModel.isLoggedIn.observe(this, Observer {
            handleLoggedIn(it)
        })

        viewModel.userImageVisible.observe(this, Observer {
            binding.appBarMainId.toolbarUserImage.visible = it
        })

        /*viewModel.scanIconVisible.observe(this, Observer {
            binding.appBarMainId.toolbarScanIcon.visible = it
        })*/

        viewModel.userImage.observe(this, Observer {
            setUserImageBitmap(it)
        })

        viewModel.userName.observe(this, Observer {
            setUserName(it)
        })

        viewModel.intError.observe(this, Observer {
            showErrorDialog(this, getString(R.string.menu), getString(it))
        })

        viewModel.stringError.observe(this, Observer {
            showErrorDialog(this, getString(R.string.menu), it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.activity_scan_menu, menu)
        val item = menu?.getItem(0)
        item?.isVisible = navController.currentDestination?.id == R.id.itemsTransactionsFragment
        return true
    }


    private fun setUserImageBitmap(bitmap: Bitmap?) {
        val navHeader = binding.navView.getHeaderView(0)
        val userImageDrawer = navHeader.findViewById<CircularImageView>(R.id.user_image_drawer)
        if (bitmap != null) {
            userImageDrawer.setImageBitmap(bitmap)
            binding.appBarMainId.toolbarUserImage.setImageBitmap(bitmap)
        }
    }

    private fun setUserName(username: String) {
        val navHeader = binding.navView.getHeaderView(0)
        val tvUserName = navHeader.findViewById<TextView>(R.id.user_name_drawer)
        tvUserName.text = username
    }

    override fun onBackPressed() {
        when {
            binding.drawerLayout.isDrawerOpen(GravityCompat.START) -> binding.drawerLayout.closeDrawer(GravityCompat.START)
            else                                                   -> super.onBackPressed()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }
}