package com.ionScanApp

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.ionScanApp.data.local.entity.LoginEntity
import com.ionScanApp.data.local.entity.UserEntity
import com.ionScanApp.data.remote.api.response.convertToUserEntity
import com.ionScanApp.data.remote.repository.LoginRepository
import com.ionScanApp.ui.BaseViewModel
import com.ionScanApp.ui.common.utils.ErrorHandler
import com.ionScanApp.ui.common.utils.ioThread
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class MainActivityViewModel(private val loginRepository: LoginRepository) : BaseViewModel() {

    private val maybeLoginEntity: LoginEntity? = loginRepository.getLoginMaybe()
        .subscribeOn(Schedulers.io())
        .blockingGet()

    private val maybeUserEntity: UserEntity? = loginRepository.getUserMaybe()
        .subscribeOn(Schedulers.io())
        .blockingGet()


    private val loginEntity = MutableLiveData<LoginEntity>()

    val userEntity = MutableLiveData<UserEntity>()

    val isLoggedIn = MediatorLiveData<Boolean>().also { result ->
        result.addSource(loginEntity) { result.value = isLoggedIn() }
    }

    val userImageVisible = MutableLiveData<Boolean>()
    val scanIconVisible = MutableLiveData<Boolean>()

    val userImage = MutableLiveData<Bitmap?>()

    val userName = MutableLiveData<String>()

    init {
        getLoginEntityFlowable()
        getUserEntityFlowable()
    }

    private fun isLoggedIn(): Boolean {
        loginEntity.value?.let {
            return it.password != null
        }
        return false
    }

    fun shouldGoToLogin(): Boolean {
        return if (needToLogIn()) {
            if (canLogIn()) {
                performLogin()
                false
            } else {
                true
            }
        } else {
            false
        }
    }

    private fun needToLogIn(): Boolean {
        return maybeUserEntity == null //|| maybeUserPermissions == null || maybeUserPermissions.isEmpty()
    }

    private fun canLogIn(): Boolean {
        maybeLoginEntity?.let {
            return it.password != null
        }
        return false
    }

    private fun performLogin() = launch {
        loginRepository.performLogin()
            .subscribeOn(Schedulers.io())
            .doOnSuccess {
                val userEntity = convertToUserEntity(it)
                loginRepository.insertUser(userEntity)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onError = { ErrorHandler.handle(it, intError, stringError) })
    }

    private fun getLoginEntityFlowable() = launch {
        loginRepository.getLoginFlowable()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ handleLoginEntity(it) }, { Timber.e(it) })
    }

    private fun handleLoginEntity(login: LoginEntity) {
        loginEntity.value = login
    }

    private fun getUserEntityFlowable() = launch {
        loginRepository.getUserFlowable()
            .subscribeBy(onNext = { handleUserEntity(it) }, onError = { Timber.e(it) })
    }

    private fun handleUserEntity(user: UserEntity) {
        userEntity.postValue(user)
        userImage.postValue(getImageBitmap(user.ImageFile))
        userName.postValue("${user.FirstName} ${user.LastName}")
    }

    fun logout() {
        ioThread {
            loginRepository.logout(loginEntity.value!!)
        }
    }

    private fun getImageBitmap(imageString: String?): Bitmap? {
        lateinit var bitmap: Bitmap

        if (imageString == null) return null
        if (imageString.indexOf(",") == -1) return null

        val base64String = imageString.split(",")[1]
        try {
            val bytes = Base64.decode(base64String, Base64.DEFAULT)
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        } catch (e: Exception) {
            Timber.e(e, "Error decoding image String: %s\n %s", e.message, imageString)
        }
        return bitmap
    }
}
