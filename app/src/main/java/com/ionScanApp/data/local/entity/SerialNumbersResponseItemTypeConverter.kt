package com.ionScanApp.data.local.entity

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class SerialNumbersResponseItemTypeConverter {

    companion object {

        @TypeConverter
        @JvmStatic
        fun fromSerialNumbersItemsList(list: List<SerialNumbersResponseItem?>?): String? {
            if (list == null) {
                return null
            }
            val gson = Gson()
            val type = object : TypeToken<List<SerialNumbersResponseItem?>?>() {

            }.type
            return gson.toJson(list, type)
        }

        @TypeConverter
        @JvmStatic
        fun toSerialNumbersItemsList(valuesString: String?): List<SerialNumbersResponseItem?>? {
            if (valuesString == null) {
                return null
            }
            val gson = Gson()
            val type = object : TypeToken<List<SerialNumbersResponseItem?>?>() {

            }.type
            return gson.fromJson(valuesString, type)
        }
    }
}