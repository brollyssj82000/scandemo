package com.ionScanApp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ionScanApp.data.local.dao.LoginDao
import com.ionScanApp.data.local.entity.LoginEntity


@Database(entities = [LoginEntity::class], version = 1, exportSchema = false)

abstract class LoginIonScanDatabase : RoomDatabase() {

    abstract fun loginDao(): LoginDao

}