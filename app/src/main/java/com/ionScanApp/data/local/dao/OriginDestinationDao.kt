package com.ionScanApp.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ionScanApp.data.local.entity.OriginDestinationEntity
import io.reactivex.Single

@Dao
interface OriginDestinationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(item: OriginDestinationEntity)

    @Query("SELECT * FROM origin_destination WHERE category_type_id=:category_type")
    fun getSelectedOriginDestinationByCategory(category_type: Int):Single<OriginDestinationEntity>

    @Query("SELECT * FROM origin_destination")
    fun getSelectedOriginDestination():Single<OriginDestinationEntity>

    @Query("DELETE FROM origin_destination")
    fun deleteDatabaseEntries()

}