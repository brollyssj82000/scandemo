package com.ionScanApp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*

import com.ionScanApp.data.local.entity.LoginEntity

import io.reactivex.Flowable
import io.reactivex.Maybe


@Dao
interface LoginDao {

    @Query("SELECT * FROM login")
    fun getLogin(): LiveData<LoginEntity>

    @Query("SELECT * FROM login LIMIT 1")
    fun getLoginFlowable(): Flowable<LoginEntity>

    @Query("SELECT * FROM login LIMIT 1")
    fun getLoginMaybe(): Maybe<LoginEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLogin(loginEntity: LoginEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateLogin(loginEntity: LoginEntity)

    @Query("DELETE FROM login")
    fun deleteAllLoginEntities()

    @Query("UPDATE login SET password = NULL WHERE id = :loginId")
    fun deleteUserPassword(loginId: Long)

    @Query("SELECT * FROM login LIMIT 1")
    fun isLoggedIn(): Maybe<LoginEntity>

    @Query("SELECT * FROM login LIMIT 1")
    fun isLoggedInFlowable(): Flowable<LoginEntity>
}
