package com.ionScanApp.data.local.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlin.random.Random

@Entity(tableName = "serial_numbers_item", indices = [(Index(value = ["uniqueId"], unique = true))])
data class SerialNumbersResponseItem(

	@field:SerializedName("Id")
	var id: Int,

	@field:SerializedName("ItemTransactionId")
	var itemTransactionId: Int,

	@field:SerializedName("SerialNumber")
	var serialNumber: String,

	@field:SerializedName("Select")
	var selected: Boolean = serialNumber.isNotBlank(),

	@field:SerializedName("UniqueId")
	@PrimaryKey
	var uniqueId: Int
)
