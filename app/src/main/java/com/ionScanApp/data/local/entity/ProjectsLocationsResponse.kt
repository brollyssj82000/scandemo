package com.ionScanApp.data.local.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "projects_locations", indices = [(Index(value = ["id"], unique = true))])
data class ProjectsLocationsResponse(

    @field:SerializedName("Level") val level: Int?,

    @field:SerializedName("ShortName") val shortName: String?,

    @field:SerializedName("Id") @PrimaryKey val id: Int?,

    @field:SerializedName("Disabled") val disabled: Boolean?,

    @field:SerializedName("Code") val code: String?,

    @field:SerializedName("Name") val name: String?)