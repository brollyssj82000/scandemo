package com.ionScanApp.data.local.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.Relation
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName

@Entity(tableName = "serial_numbers_response", indices = [(Index(value = ["id"], unique = true))])

data class SerialNumbersResponse(

	@field:SerializedName("Id")
	@PrimaryKey
	var id: Int?,

	@field:SerializedName("Destination")
	var destination: Int?,

	@TypeConverters(SerialNumbersResponseItemTypeConverter::class)
	@field:SerializedName("SerialNumbers")
	@Ignore
	var serialNumbers: List<SerialNumbersResponseItem>?,

	@field:SerializedName("ItemSerialNumber")
	var itemSerialNumber: Boolean?,

	@field:SerializedName("From")
	var fromSource: Int?,

	@field:SerializedName("ItemId")
	var itemId: Int?,

	@field:SerializedName("Source")
	var source: Int?,

	@field:SerializedName("FromProjectCostId")
	var fromProjectCostId: Int?,

	@field:SerializedName("FromPurchaseQuoteLineId")
	var fromPurchaseQuoteLineId: Int?,

	@field:SerializedName("ToProjectCostId")
	var toProjectCostId: Int?,

	@field:SerializedName("ItemItemId")
	var itemItemId: String?,

	@field:SerializedName("ToBookAmount")
	var toBookAmount: Int?,

	@field:SerializedName("ItemName")
	var itemName: String?,

	@field:SerializedName("To")
	var toDest: Int?,

	@field:SerializedName("FromEntityId")
	var fromEntityId: Int?,

	@field:SerializedName("ToEntityId")
	var toEntityId: Int?
)
{
	constructor() : this(null,null,null,null,null,null,null, null,null,null,null,null,null,null,null,null)
}