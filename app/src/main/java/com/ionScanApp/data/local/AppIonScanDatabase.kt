package com.ionScanApp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ionScanApp.data.local.dao.*
import com.ionScanApp.data.local.entity.*
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.data.local.entity.ProjectsLocationsResponse
import com.ionScanApp.data.local.entity.SerialNumbersResponse

@Database(
    entities = [LoginEntity::class, UserEntity::class, ItemTransactionsResponse::class, ProjectsLocationsResponse::class, OriginDestinationEntity::class, SerialNumbersResponse::class,
                SerialNumbersResponseItem::class],
    version = 38,
    exportSchema = false)

@TypeConverters(SerialNumbersResponseItemTypeConverter::class)

abstract class AppIonScanDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun projectsStockLocationsDao(): ProjectsLocationsDao

    abstract fun itemTransactionsDao(): ItemTransactionsDao

    abstract fun originDestinationDao(): OriginDestinationDao

    abstract fun serialNumbersDao(): SerialNumbersDao
}