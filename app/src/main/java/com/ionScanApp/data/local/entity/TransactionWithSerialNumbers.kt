package com.ionScanApp.data.local.entity

import androidx.room.PrimaryKey
import androidx.room.Relation
import com.google.gson.annotations.SerializedName

data class TransactionWithSerialNumbers(

    @field:SerializedName("Id")
    @PrimaryKey
    var id: Int?,

    @field:SerializedName("Destination")
    var destination: Int?,

    @field:SerializedName("ItemSerialNumber")
    var itemSerialNumber: Boolean?,

    @field:SerializedName("From")
    var fromSource: Int?,

    @field:SerializedName("ItemId")
    var itemId: Int?,

    @field:SerializedName("Source")
    var source: Int?,

    @field:SerializedName("ItemItemId")
    var itemItemId: String?,

    @field:SerializedName("ToBookAmount")
    var toBookAmount: Int?,

    @field:SerializedName("ItemName")
    var itemName: String?,

    @field:SerializedName("To")
    var toDest: Int?,

    @field:SerializedName("FromEntityId")
    var fromEntityId: Int?,

    @field:SerializedName("ToEntityId")
    var toEntityId: Int?,

    @Relation(parentColumn = "id", entity = SerialNumbersResponseItem::class, entityColumn = "itemTransactionId")
    val SerialNumbers: List<SerialNumbersResponseItem>
)