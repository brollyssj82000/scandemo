package com.ionScanApp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface ItemTransactionsDao {

    @Transaction
    fun clearDatabaseThenInsertNewItems(items: List<ItemTransactionsResponse>){
        deleteDatabaseEntries()
        insertItems(items)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItems(items: List<ItemTransactionsResponse>)

    @Query("UPDATE item_transactions SET toBookAmount=:toBookValue WHERE id = :id")
    fun updateToBookItemTransaction(toBookValue: Int?, id: Int?)

    @Query("SELECT * FROM item_transactions")
    fun getItemTransactionsPaginated(): DataSource.Factory<Int, ItemTransactionsResponse>

    @Query("SELECT * FROM item_transactions")
    fun getItemTransactionsDB(): Flowable<List<ItemTransactionsResponse>>

    @Query("SELECT * FROM item_transactions WHERE id= :id")
    fun getItemTransaction(id: Int): Single<List<ItemTransactionsResponse>>

    @Query("DELETE FROM item_transactions")
    fun deleteDatabaseEntries()

    @Query("DELETE FROM item_transactions WHERE id= :id")
    fun deleteDatabaseEntry(id: Int)

    @Query("DELETE FROM item_transactions WHERE id != :id")
    fun deleteDatabaseEntryThatDontMatchID(id: Int?)
}