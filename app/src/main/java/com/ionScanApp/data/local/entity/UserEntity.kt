package com.ionScanApp.data.local.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Ignore
import androidx.room.Index


@Entity(tableName = "user",
        primaryKeys = ["Id"],
        indices = [Index(value = ["Id", "RoleId"], unique = true)]
)
data class UserEntity(
        val Id:         Int,
        val RoleId:     Int,
        val FirstName:  String,
        val LastName:   String,
        val Language:   String,
        val ImageFile:  String?
) {
    @Ignore
    constructor(RoleId: Int = 0, FirstName: String = "", LastName: String = "", Language: String = "", ImageFile: String? = null)
            : this(0, RoleId, FirstName, LastName, Language, ImageFile)
}