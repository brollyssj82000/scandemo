package com.ionScanApp.data.local.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "item_transactions", indices = [(Index(value = ["id"], unique = true))])
data class ItemTransactionsResponse(

    @field:SerializedName("Id")
    @PrimaryKey
    var id: Int,

    @field:SerializedName("Destination")
	var destination: Int?,

    @field:SerializedName("OnHold")
	var onHold: Boolean?,

    @field:SerializedName("DateRequired")
	var dateRequired: String?,

    @field:SerializedName("ItemSerialNumber")
	var itemSerialNumber: Boolean?,

    @field:SerializedName("SourceName")
	var sourceName: String?,

    @field:SerializedName("From")
	var from: Int?,

    @field:SerializedName("BookedAmount")
	var bookedAmount: Int?,

    @field:SerializedName("ItemId")
	var itemId: Int?,

    @field:SerializedName("Source")
	var source: Int?,

    @field:SerializedName("FromPurchaseQuoteLineId")
	var fromPurchaseQuoteLineId: Int?,

    @field:SerializedName("FromProjectCostId")
	var fromProjectCostId: Int?,

    @field:SerializedName("ToProjectCostId")
	var toProjectCostId: Int?,

    @field:SerializedName("UndeliveredAmount")
	var undeliveredAmount: Int?,

	@field:SerializedName("MinimumStock")
	var minimumStock: Int?,

	@field:SerializedName("ToBePicked")
	var toBePicked: Boolean?,

    @field:SerializedName("Memo")
	var memo: String?,

    @field:SerializedName("ItemItemId")
	var itemItemId: String?,

    @field:SerializedName("ToBookAmount")
	var toBookAmount: Int?,

    @field:SerializedName("DestinationName")
	var destinationName: String?,

    @field:SerializedName("ItemName")
	var itemName: String?,

    @field:SerializedName("To")
	var to: Int?,

    @field:SerializedName("RequiredAmount")
	var requiredAmount: Int?,

    @field:SerializedName("RemainingAmount")
	var remainingAmount: Int?,

    @field:SerializedName("QuantityInStock")
	var quantityInStock: Int?,

	@field:SerializedName("FromEntityId")
	var fromEntityId: Int?,

	@field:SerializedName("ToEntityId")
	var toEntityId: Int?
)