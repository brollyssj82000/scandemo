package com.ionScanApp.data.local.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "origin_destination", indices = [(Index(value = ["id"], unique = true))])
data class OriginDestinationEntity(

    @PrimaryKey(autoGenerate = true)
    val id: Int? = 0,

    val category_type_id: Int?,

    val originId: Int?,

    val originName: String?,

    val destinationId: Int?,

    val destinationName: String?
)
{
constructor(
category_type_id: Int?,

originId: Int?,

originName: String?,

destinationId: Int?,

destinationName: String?) : this(0, category_type_id = category_type_id, originId = originId, originName = originName, destinationId = destinationId, destinationName = destinationName)
}