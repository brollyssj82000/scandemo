package com.ionScanApp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.ionScanApp.data.local.entity.UserEntity
import io.reactivex.Flowable
import io.reactivex.Maybe


@Dao
interface UserDao {

    @Query("SELECT * FROM user") fun getUser(): UserEntity

    @Query("SELECT * FROM user") fun getUserLiveData(): LiveData<UserEntity>

    @Query("SELECT * FROM user") fun getUserMaybe(): Maybe<UserEntity>

    @Query("SELECT * FROM user") fun getUserFlowable(): Flowable<UserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE) fun insertUser(userEntity: UserEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE) fun updateUser(userEntity: UserEntity)

    @Query("DELETE FROM user") fun deleteUser()

}
