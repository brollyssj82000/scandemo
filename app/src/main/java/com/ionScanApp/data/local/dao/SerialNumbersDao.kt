package com.ionScanApp.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.ionScanApp.data.local.entity.SerialNumbersResponse
import com.ionScanApp.data.local.entity.SerialNumbersResponseItem
import com.ionScanApp.data.local.entity.TransactionWithSerialNumbers
import io.reactivex.Flowable

@Dao
interface SerialNumbersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSerialNumbersResponse(items: List<SerialNumbersResponse>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertSerialNumbersItems(items: List<SerialNumbersResponseItem>?)

    @Transaction
    @Query("SELECT toEntityId, fromEntityId, toDest, fromSource, itemItemId, itemId, destination, itemSerialNumber, toBookAmount, itemName, id, source FROM serial_numbers_response")
    fun getTransactionWIthSerialNumbers(): Flowable<List<TransactionWithSerialNumbers>>

    @Query("SELECT * FROM serial_numbers_response")
    fun getSerialNumbers(): Flowable<List<SerialNumbersResponse>>

    @Query("UPDATE serial_numbers_item SET serialNumber = :serialNumber WHERE uniqueId = :uniqueId")
    fun updateSerialNumber(serialNumber: String?, uniqueId: Int)

    @Query("UPDATE serial_numbers_item SET selected = :isSelected WHERE uniqueId = :uniqueId")
    fun updateSerialNumberIsSelected(uniqueId: Int, isSelected: Int)

    @Query("DELETE FROM serial_numbers_item WHERE selected = :isSelected ")
    fun deleteUnselectedSerialNumbers(isSelected: Int)

    @Query("DELETE FROM serial_numbers_item")
    fun deleteSerialNumberItems()

    @Query("DELETE FROM serial_numbers_response")
    fun deleteSerialNumberResponse()
}