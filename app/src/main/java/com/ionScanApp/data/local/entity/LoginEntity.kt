package com.ionScanApp.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey


@Entity(tableName = "login")
data class LoginEntity @Ignore constructor(
        @PrimaryKey()
        @ColumnInfo(name = "id")        var id:         Long    = 0,
        @ColumnInfo(name = "username")  var username:   String?  = "",
        @ColumnInfo(name = "password")  var password:   String?  = "",
        @ColumnInfo(name = "url")       var url:        String?  = "",
        @ColumnInfo(name = "success")   var success:    Boolean? = false
) {
    constructor(): this(0, "", "", "", false)
}
