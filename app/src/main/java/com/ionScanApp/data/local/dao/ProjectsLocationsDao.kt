package com.ionScanApp.data.local.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ionScanApp.data.local.entity.ProjectsLocationsResponse
import io.reactivex.Single

@Dao
interface ProjectsLocationsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE) abstract
    fun insertItems(notes: List<ProjectsLocationsResponse>)

    @Query("SELECT * FROM projects_locations") abstract
    fun getProjectsStockLocations(): DataSource.Factory<Int, ProjectsLocationsResponse>

    @Query("DELETE FROM projects_locations") abstract
    fun deleteDatabaseEntries()
}