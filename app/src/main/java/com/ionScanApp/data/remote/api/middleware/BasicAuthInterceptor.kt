package com.ionScanApp.data.remote.api.middleware

import android.util.Base64

import com.ionScanApp.data.local.dao.LoginDao
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import timber.log.Timber


class BasicAuthInterceptor(private val loginDao: LoginDao) : Interceptor {

    companion object {
        const val AUTHORIZATION_HEADER = "Authorization"
    }

    override fun intercept(chain: Chain): Response {
        val originalRequest = chain.request()

        val loginEntity = loginDao.getLoginMaybe().blockingGet()

        val username = loginEntity?.username
        val password = loginEntity?.password

        Timber.d("credentials = $username:$password")
        if (!username.isNullOrBlank() && !password.isNullOrBlank()) {

            val credentials = "$username:$password"
            val base64 = Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            val basic = "Basic $base64"

            val authorisedRequest = originalRequest.newBuilder()
                .header(AUTHORIZATION_HEADER, basic)
                .build()

            return chain.proceed(authorisedRequest)
        }

        return chain.proceed(originalRequest)
    }
}