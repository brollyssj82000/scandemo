package com.ionScanApp.data.remote.api.middleware

import android.webkit.URLUtil
import com.ionScanApp.data.local.dao.LoginDao
import okhttp3.Interceptor
import okhttp3.Response


class HostnameInterceptor(private val loginDao: LoginDao) : Interceptor {

    companion object {
        const val DEFAULT_SCHEME = "https"
        const val DEFAULT_HOST = "localhost"
    }
    private var hostName = DEFAULT_HOST
    private var scheme = DEFAULT_SCHEME

    private fun setHostAndScheme(userUrl: String?) {

        userUrl?.let {
            if (URLUtil.isHttpUrl(userUrl) || URLUtil.isHttpsUrl(userUrl)) {
                val (scheme, hostName) = userUrl.split("://")
                this.scheme = scheme
                this.hostName = hostName
            } else {
                this.scheme = DEFAULT_SCHEME
                this.hostName = userUrl
            }
        }
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val userUrl = loginDao.getLoginMaybe()
            .map { it.url }
            .blockingGet() ?: DEFAULT_HOST

        setHostAndScheme(userUrl)

        val url = chain.request().url.newBuilder()
            .scheme(scheme)
            .host(hostName)
            .build()

        val request = chain.request().newBuilder().url(url).build()

        return chain.proceed(request)
    }
}
