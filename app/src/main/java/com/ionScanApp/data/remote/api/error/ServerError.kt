package com.ionScanApp.data.remote.api.error

import com.google.gson.annotations.SerializedName

data class ServerError(
    @SerializedName("Message")
    val message: String?,
    @SerializedName("MessageDetail")
    val messageDetail: String?
)
