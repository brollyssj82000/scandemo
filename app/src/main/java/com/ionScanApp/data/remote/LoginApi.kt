package com.ionScanApp.data.remote

import com.ionScanApp.data.remote.api.response.UserResponse
import io.reactivex.Single
import retrofit2.http.GET


interface LoginApi {

    @GET("api/Resources/GetMeLight")
    fun getMeLightSingle(): Single<UserResponse>

}
