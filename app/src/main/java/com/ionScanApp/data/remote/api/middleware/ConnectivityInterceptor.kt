package com.ionScanApp.data.remote.api.middleware

import android.content.Context
import com.ionScanApp.ui.common.utils.isOnline
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import java.io.IOException


class ConnectivityInterceptor(private val mContext: Context) : Interceptor {

    override fun intercept(chain: Chain): Response {
        if (!isOnline(mContext)) {
            throw IOException()
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }
}