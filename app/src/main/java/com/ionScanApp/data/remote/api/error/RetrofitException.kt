package com.ionScanApp.data.remote.api.error

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response
import retrofit2.Retrofit
import timber.log.Timber
import java.io.IOException


class RetrofitException(private val _message: String?,
                        private val _code: Int?,
                        private val _url: String?,
                        private val _response: Response<*>?,
                        private val _kind: Kind,
                        private val _exception: Throwable?,
                        private val _retrofit: Retrofit?
) : RuntimeException(_message, _exception) {

    private var _errorData : ServerError? = null

    companion object {

        fun toHttpError(code: Int, url: String, response: Response<*>): RetrofitException {
            val message = response.code().toString() + " " + response.message()
            return RetrofitException(message, code, url, response, Kind.HTTP, null, null)
        }

        fun toHttpError(code: Int, url: String, message: String?): RetrofitException {
            return RetrofitException(message, code, url, null, Kind.HTTP, null, null)
        }

        fun toServerError(code: Int, serverError: ServerError): RetrofitException {
            return RetrofitException(serverError.message, code, null, null, Kind.SERVER, null, null)
        }

        fun toServerInitializingError(): RetrofitException {
            return RetrofitException(null, 200, null, null, Kind.SERVER_INIT, null, null)
        }

        fun toNetworkError(exception: IOException): RetrofitException {
            return RetrofitException(exception.message, null, null, null, Kind.NETWORK, exception, null)
        }

        fun toConnectivityError(): RetrofitException {
            return RetrofitException(null, null, null, null, Kind.CONNECTIVITY, null, null)
        }

        fun toTimeoutError(): RetrofitException {
            return RetrofitException(null, null, null, null, Kind.TIMEOUT, null, null)
        }

        fun toUnexpectedError(exception: Throwable): RetrofitException {
            return RetrofitException(exception.message, null, null, null, Kind.UNEXPECTED, exception, null)
        }
    }


    fun getErrorMessage() = _message

    fun getCode() = _code

    /** The request URL which produced the error. */
    fun getUrl() = _url

    /** Response object containing status code, headers, body, etc. */
    fun getResponse() = _response

    /** The event kind which triggered this error. */
    fun getKind() = _kind

    /** The Retrofit this request was executed on */
    fun getRetrofit() = _retrofit

    /** The data returned from the server in the response body*/
    fun getErrorData() : ServerError?  = _errorData

    private fun deserializeServerError() {
        if (_response?.errorBody() != null) {
            try {
                _errorData =  getErrorBodyAs(ServerError::class.java)
            } catch (e: IOException) {
                Timber.e(e)
            }
        }
    }

    /**
     * HTTP response body converted to specified `type`. `null` if there is no
     * response.
     * @throws IOException if unable to convert the body to the specified `type`.
     */
    @Throws(IOException::class)
    fun <T> getErrorBodyAs(type: Class<T>): T? {
        if (_response?.errorBody() == null || _retrofit == null) {
            return null
        }
        val converter : Converter<ResponseBody, T> =
            _retrofit.responseBodyConverter(type, arrayOfNulls<Annotation>(0))
        return converter.convert(_response.errorBody()!!)
    }

    enum class Kind {
        /** An [IOException] occurred while communicating to the server.  */
        NETWORK,
        CONNECTIVITY,
        TIMEOUT,
        /** A non-200 HTTP status code was received from the server.  */
        HTTP,
        //        HTTP_NON_JSON,
        SERVER,
        SERVER_INIT,
//        HTTP_422_WITH_DATA,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }
}
