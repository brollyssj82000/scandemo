package com.ionScanApp.data.remote.repository

import com.ionScanApp.data.remote.ItemTransactionsApi
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.data.local.entity.SerialNumbersResponse
import com.ionScanApp.data.local.entity.TransactionWithSerialNumbers
import io.reactivex.Flowable
import io.reactivex.Single

interface ItemTransactionsRepository {
    fun getItemTransactions(from: Int, source: Int?, to: Int, destination: Int?, page: Int, pageSize: Int): Flowable<List<ItemTransactionsResponse>>
    fun bookItem(bookingItemRequest: List<TransactionWithSerialNumbers>): Flowable<List<ItemTransactionsResponse>>
    fun getSerialNumbers(serialNumber: List<ItemTransactionsResponse>): Flowable<List<SerialNumbersResponse>>
}

class ItemTransactionsRepositoryImpl(private val itemTransactionsApi: ItemTransactionsApi) : ItemTransactionsRepository {

    override fun getItemTransactions(from: Int, source: Int?, to: Int, destination: Int?, page: Int, pageSize: Int): Flowable<List<ItemTransactionsResponse>> {
        return itemTransactionsApi.getItemTransactions(from, source, to, destination, page, pageSize)
    }

    override fun bookItem(bookingItemRequest: List<TransactionWithSerialNumbers>): Flowable<List<ItemTransactionsResponse>> {
        return itemTransactionsApi.postBookedItem(bookingItemRequest)
    }

    override fun getSerialNumbers(serialNumber: List<ItemTransactionsResponse>): Flowable<List<SerialNumbersResponse>> {
        return itemTransactionsApi.getSerialNumbersForTransaction(serialNumber)
    }
}