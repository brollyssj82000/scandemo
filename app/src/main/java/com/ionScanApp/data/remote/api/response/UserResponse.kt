package com.ionScanApp.data.remote.api.response

import com.ionScanApp.data.local.entity.UserEntity


data class UserResponse(
    val Id: Int,
    val RoleId: Int,
    val FirstName: String?,
    val LastName: String?,
    val Language: String?,
    val ImageFile: String?
)

fun convertToUserEntity(userResponse: UserResponse): UserEntity = with(userResponse) {
    return UserEntity(Id, RoleId, FirstName!!, LastName!!, Language!!, ImageFile)
}
