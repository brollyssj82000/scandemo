package com.ionScanApp.data.remote

import com.ionScanApp.data.local.entity.ProjectsLocationsResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface StockLocationsProjectsApi {

    @GET("api/StockLocations/GetBasicList")
    fun getStockLocations( @Query("page") page: Int,
                           @Query("pageSize") pageSize: Int,
                           @Query("onlyParents ") onlyParents : Boolean = true,
                           @Query("q") q: String) : Observable<List<ProjectsLocationsResponse>>

    @GET("api/Projects/GetProjectsNames")
    fun getProjectsNames(@Query("relationId") relationId: Int,
                         @Query("componentId") componentId: Int = 1360,
                         @Query("filterByAllowTransactions") filterByAllowTransactions: Boolean = true,
                         @Query("page") page: Int,
                         @Query("pageSize") pageSize: Int,
                         @Query("q") q: String): Observable<List<ProjectsLocationsResponse>>
}