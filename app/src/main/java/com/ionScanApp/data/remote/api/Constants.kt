package com.ionBIZMobile.data.remote.api


object Components {

    const val FormRelationsList = 300
    const val FormRelationsListAddresses = 310
    const val FormRelationsListContacts = 320
    const val FormRelationsListContactsNotes = 322

    const val FormPlanningPlanning = 500

    const val FormTimeTrackingCells = 610

    const val FormLeavesYearView = 810

    const val FormCostsList = 820

    const val FormIssuesList = 1100

    const val FormHomeNotes  = 10
}

object Actions {

    const val StartDateChanged = "StartDateChanged"
    const val StartTimeChanged = "StartTimeChanged"
    const val StopDateChanged = "StopDateChanged"
    const val StopTimeChanged = "StopTimeChanged"
    const val DurationChanged = "DurationChanged"
    const val FullDaysClicked = "FullDaysClicked"

    const val ActionCancelLeave = 6
}

object PermAction {
    const val View      = 0
    const val Add       = 1
    const val Edit      = 2
    const val Delete    = 3
}

object PermLevel {
    const val Own           = 0
    const val Department    = 1
    const val All           = 2
}


object RolePermission {

    const val PlanningHideTaskPanel = 1
    const val IssueAllowToAssignUsers = 2
    const val IssueAllowToUpdateActionRequiredBy = 3
    const val TimesheetAllowToEditConfirmedTimesheets = 4
    const val TimesheetAllowToEditApprovedTimesheets = 5
    const val TimesheetAllowToEditInvoicedAndClosedTimesheets = 6
    const val CostAllowToEditInvoicedAndClosedCosts = 7
    const val TimesheetGiveAccessToBillableNotBillable = 8
    const val AllowToDeleteProjectsWithLinkedTimesheetCostOrInvoice = 9
    const val AllowToDeleteIssuesWithLinkedTimesheetOrInvoice = 10
    const val AllowToAddDeleteCostsFromActuals = 11
    const val AllowToEditIteration = 12
    const val AllowAccessToViewIssueSalesPrice = 13
    const val AllowAccesToViewIssueBillable = 14
    const val AllowToApproveOwnLeaves = 15
    const val AllowToLoginWithOtherUserAccount = 16
    const val AllowToApproveOwnPlanningItems = 17
    const val CostAllowAccessToFieldBillable = 18
    const val CostAllowAccessToFieldReimburse = 19
    const val AllowToTransferTasksToOtherProjects = 20
    const val CostAllowToEditConfirmedCosts = 21
    const val AllowToSelectFromUnassignedTasks = 22
    const val AllowToEditProjectIds = 23
    const val AllowToAddTimesheetsInConfirmedPeriods = 24
    const val BlockConfirmedPeriod = 25
    const val CostAllowToEditApprovedCosts = 26
    const val AllowToAddTimesheetsUntilAdminClosingDate = 27
    const val AllowToAddCostsUntilAdminClosingDate = 28
    const val AllowToAddInvoicesUntilAdminClosingDate = 29
    const val AllowPlanningOnSalesQuotes = 30
    const val AllowToViewTotalActualAndTaskBaseline = 31
    const val ActivityOnTasksIsNotRequired = 32
    const val ActivityTypeOnTasksIsNotRequired = 33
    const val AllowToSelectFromAllProjectsOnPlanning = 34
    const val AllowToSelectTasksOutOfPeriodOnPlanning = 35
    const val AllowPlanningDailyAutomaticEmailNotification = 36
    const val AllowCostsheetUnlock = 37
    const val AllowToApproveAllCosts = 38
    const val AllowAccessToHourlyRates = 39
    const val AllowProjectBaselineUnlock = 40
    const val AllowShowBaselineAndTotalActualsMyTask = 41
    const val AllowToEditApprovedLeaves = 42
    const val AllowInvoiceUnlock = 43
    const val AllowAccessCosts = 44
    const val AllowAccessToCostBudget = 45
    const val AllowAccessToCustomerAndProjectCostSheetHeader = 46
    const val AllowToEditInvoiceScheduleSelectionOnTasks = 47
    const val AllowToAddDeleteCostsFromBudget = 48
    const val AllowAccessToDoneButton = 49
    const val AllowAccessToDoneButtonMyTasks = 50
    const val AllowToEditInvoiceFrom = 51
    const val AllowActualsGreaterThenAssignedAccordingToTask = 52
    const val AllowProjectBaselineUnlockProjectManager = 53
    const val AllowProjectBaselineUnlockProjectSupervisor = 54
    const val AllowProjectBaselineUnlockAccountManager = 55
    const val AllowUpdateCostBudget = 56
    const val TimesheetAllowAccessToPercentBillable = 57
}

object GroupSettingsConst {
    const val DefaultReimburse = "DefaultReimburse"
    const val IncludeBreaksOnTimesheets = "IncludeBreaksOnTimesheets"
    const val AllowTimesheetConfirming = "AllowTimesheetConfirming"
    const val HideImportPlanningButton = "HideImportPlanningButton"
}