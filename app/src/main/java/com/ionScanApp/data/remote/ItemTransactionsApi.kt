package com.ionScanApp.data.remote

import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.data.local.entity.SerialNumbersResponse
import com.ionScanApp.data.local.entity.TransactionWithSerialNumbers
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ItemTransactionsApi {

    @GET("api/ItemTransactions/GetProposedTransactions")
    fun getItemTransactions(@Query("from") from: Int,
                            @Query("source") source: Int?,
                            @Query("to") to: Int,
                            @Query("destination") destination: Int?,
                            @Query("page") page: Int,
                            @Query("pageSize") pageSize: Int): Flowable<List<ItemTransactionsResponse>>

    @POST("api/ItemTransactions/Post")
    fun postBookedItem(@Body bookingItemRequest: List<TransactionWithSerialNumbers>): Flowable<List<ItemTransactionsResponse>>

    @POST("api/ItemTransactions/GetSerialNumbers")
    fun getSerialNumbersForTransaction(@Body serialNumber: List<ItemTransactionsResponse>) : Flowable<List<SerialNumbersResponse>>
}
