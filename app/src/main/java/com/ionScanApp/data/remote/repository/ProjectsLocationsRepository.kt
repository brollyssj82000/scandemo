package com.ionScanApp.data.remote.repository

import com.ionScanApp.data.remote.StockLocationsProjectsApi
import com.ionScanApp.data.local.entity.ProjectsLocationsResponse
import io.reactivex.Observable

interface ProjectsLocationsRepository {

    fun getStockLocationsApi(page: Int, pageSize: Int, query: String): Observable<List<ProjectsLocationsResponse>>

    //fun getProjectsApi(relationId: Int, componentId: Int, page: Int, pageSize: Int, query: String, filterByNotCompletedStatus: Boolean, filterByNotClosedStatus: Boolean): Observable<List<ProjectsLocations>>

    fun getProjectsApi(relationId: Int, page: Int, pageSize: Int, query: String): Observable<List<ProjectsLocationsResponse>>
}

class StockLocationLocationsRepositoryImpl(private val stockLocationsProjectsApi: StockLocationsProjectsApi) : ProjectsLocationsRepository {

//    override fun getProjectsApi(relationId: Int, componentId: Int, page: Int, pageSize: Int, query: String, filterByNotCompletedStatus: Boolean, filterByNotClosedStatus: Boolean): Observable<List<ProjectsLocations>> {
//        return stockLocationsProjectsApi.getProjectsNames(relationId, componentId, page, pageSize, query, filterByNotCompletedStatus, filterByNotClosedStatus)
//    }

    override fun getProjectsApi(relationId: Int, page: Int, pageSize: Int, query: String): Observable<List<ProjectsLocationsResponse>> {
        return stockLocationsProjectsApi.getProjectsNames(relationId, 1360, true, page, pageSize, query)
    }

    override fun getStockLocationsApi(page: Int, pageSize: Int, query: String): Observable<List<ProjectsLocationsResponse>> {
        return stockLocationsProjectsApi.getStockLocations(page, pageSize, true, query)
    }
}