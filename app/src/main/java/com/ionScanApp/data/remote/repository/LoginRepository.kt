package com.ionScanApp.data.remote.repository

import com.ionScanApp.data.local.dao.LoginDao
import com.ionScanApp.data.local.dao.UserDao
import com.ionScanApp.data.local.entity.LoginEntity
import com.ionScanApp.data.local.entity.UserEntity
import com.ionScanApp.data.remote.LoginApi
import com.ionScanApp.data.remote.api.response.UserResponse
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single


interface LoginRepository {

    fun getUserFlowable(): Flowable<UserEntity>
    fun getUserMaybe(): Maybe<UserEntity>
    fun insertUser(userEntity: UserEntity)
    fun getLoginMaybe(): Maybe<LoginEntity>
    fun getLoginFlowable(): Flowable<LoginEntity>
    fun deleteLogin()
    fun insertLogin(loginEntity: LoginEntity)
    fun logout(loginEntity: LoginEntity)
    fun performLogin(): Single<UserResponse>
}

class LoginRepositoryImpl(private val userDao: UserDao, private val loginDao: LoginDao, private val loginApi: LoginApi) : LoginRepository {

    override fun getUserFlowable(): Flowable<UserEntity> {
        return userDao.getUserFlowable()
    }

    override fun getUserMaybe(): Maybe<UserEntity> {
        return userDao.getUserMaybe()
    }

    override fun insertUser(userEntity: UserEntity) {
        userDao.insertUser(userEntity)
    }

    override fun getLoginMaybe(): Maybe<LoginEntity> {
        return loginDao.getLoginMaybe()
    }

    override fun getLoginFlowable(): Flowable<LoginEntity> {
        return loginDao.getLoginFlowable()
    }

    override fun deleteLogin() {
        loginDao.deleteAllLoginEntities()
    }

    override fun insertLogin(loginEntity: LoginEntity) {
        loginDao.insertLogin(loginEntity)
    }

    override fun logout(loginEntity: LoginEntity) {
        //  roleDao.deleteAllRoles()
        loginDao.deleteUserPassword(loginEntity.id)
    }

    override fun performLogin(): Single<UserResponse> {
        return loginApi.getMeLightSingle()
    }
}