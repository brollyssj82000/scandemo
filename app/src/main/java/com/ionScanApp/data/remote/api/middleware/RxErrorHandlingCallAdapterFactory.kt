package com.ionScanApp.data.remote.api.middleware

import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.stream.MalformedJsonException
import com.ionScanApp.data.remote.api.error.RetrofitException
import com.ionScanApp.data.remote.api.error.ServerError
import io.reactivex.*
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.net.SocketTimeoutException


class RxErrorHandlingCallAdapterFactory private constructor() : CallAdapter.Factory() {

    companion object {

        fun create(): CallAdapter.Factory = RxErrorHandlingCallAdapterFactory()
    }

    private val original: RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.createAsync()

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *> {
        val original = original.get(returnType, annotations, retrofit)!! as CallAdapter<Any, Any>
        return RxCallAdapterWrapper(returnType, original)
    }

    /**
     * RxCallAdapterWrapper
     */
    internal inner class RxCallAdapterWrapper(private val returnType: Type,
                                              private val wrapped: CallAdapter<Any, Any>) : CallAdapter<Any, Any> {

        override fun responseType(): Type {
            return wrapped.responseType()
        }

        override fun adapt(call: Call<Any>): Any? {
            val rawType = CallAdapter.Factory.getRawType(returnType)

            val isFlowable      = rawType == Flowable::class.java
            val isSingle        = rawType == Single::class.java
            val isMaybe         = rawType == Maybe::class.java
            val isCompletable   = rawType == Completable::class.java
            if (rawType != Observable::class.java && !isFlowable && !isSingle && !isMaybe) {
                return null
            }
            if (returnType !is ParameterizedType) {
                val name = when {
                    isFlowable  -> "Flowable"
                    isSingle    -> "Single"
                    isMaybe     -> "Maybe"
                    else        -> "Observable"
                }
                throw IllegalStateException("$name return type must be parameterized as $name<Foo> or $name<? extends Foo>")
            }

            if (isFlowable) {
                return (wrapped.adapt(call) as Flowable<*>).onErrorResumeNext { throwable: Throwable ->
                    Flowable.error(convertToBaseException(throwable))
                }
            }
            if (isSingle) {
                return (wrapped.adapt(call) as Single<*>).onErrorResumeNext { throwable ->
                    Single.error(convertToBaseException(throwable))
                }
            }
            if (isMaybe) {
                return (wrapped.adapt(call) as Maybe<*>).onErrorResumeNext { throwable: Throwable ->
                    Maybe.error(convertToBaseException(throwable))
                }
            }
            if (isCompletable) {
                return (wrapped.adapt(call) as Completable).onErrorResumeNext { throwable ->
                    Completable.error(convertToBaseException(throwable))
                }
            }
            return (wrapped.adapt(call) as Observable<*>).onErrorResumeNext { throwable: Throwable ->
                Observable.error(convertToBaseException(throwable))
            }
        }

        private fun convertToBaseException(throwable: Throwable): RetrofitException {
            if (throwable is RetrofitException) {
                return throwable
            }

            if (throwable is MalformedJsonException) {
                return RetrofitException.toServerInitializingError()
            }

            if (throwable is SocketTimeoutException) {
                return RetrofitException.toTimeoutError()
            }

            if (throwable is IOException) {
                return RetrofitException.toNetworkError(throwable)
            }

            if (throwable is HttpException) {
                val response = throwable.response()
                val code = response?.code()
                val url = response?.raw()?.request?.url.toString()
                val errorString = response?.errorBody()?.string()
                return if (errorString != null) {
                    try {
                        val serverError: ServerError? = Gson().fromJson(errorString, ServerError::class.java)
                        if (serverError != null && !TextUtils.isEmpty(serverError.message)) {
                            RetrofitException.toServerError(code!!, serverError)
                        } else {
                            RetrofitException.toServerError(code!!, ServerError(errorString, ""))
                        }
                    } catch (e: JsonSyntaxException) {
                        RetrofitException.toServerError(code!!, ServerError(errorString, ""))
                    } catch (e: Throwable) {
                        RetrofitException.toHttpError(code!!, url, errorString)
                    }

                } else {
                    RetrofitException.toHttpError(code!!, url, response)
                }
            }

            return RetrofitException.toUnexpectedError(throwable)
        }
    }
}
