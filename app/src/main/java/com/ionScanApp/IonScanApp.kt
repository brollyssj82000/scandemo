package com.ionScanApp

import android.app.Application
import android.util.Log
import androidx.camera.camera2.Camera2Config
import androidx.camera.core.CameraXConfig
import com.facebook.stetho.Stetho
import com.ionScanApp.di.databaseModule
import com.ionScanApp.di.ionScanModule
import com.ionScanApp.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class IonScanApp : Application(), CameraXConfig.Provider{

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@IonScanApp)
            modules(ionScanModule + networkModule + databaseModule)
        }

        loggingActivation()

        stethoActivation()
    }

    private fun stethoActivation() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

      private fun loggingActivation() {
          if (BuildConfig.DEBUG) {
              Timber.plant(Timber.DebugTree())
          } else {
              Timber.plant(CrashReportingTree())
          }
      }

    override fun getCameraXConfig(): CameraXConfig {
        return Camera2Config.defaultConfig()
    }
}


private class CrashReportingTree : Timber.Tree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        when (priority) {
            Log.VERBOSE, Log.DEBUG, Log.INFO, Log.WARN -> return
        }
    }
}
