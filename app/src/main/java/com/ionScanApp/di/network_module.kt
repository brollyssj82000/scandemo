package com.ionScanApp.di

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.ionScanApp.BuildConfig
import com.ionScanApp.data.local.dao.LoginDao
import com.ionScanApp.data.remote.ItemTransactionsApi
import com.ionScanApp.data.remote.LoginApi
import com.ionScanApp.data.remote.StockLocationsProjectsApi
import com.ionScanApp.data.remote.api.middleware.BasicAuthInterceptor
import com.ionScanApp.data.remote.api.middleware.ConnectivityInterceptor
import com.ionScanApp.data.remote.api.middleware.HostnameInterceptor
import com.ionScanApp.data.remote.api.middleware.RxErrorHandlingCallAdapterFactory
import com.ionScanApp.di.NetworkProperties.TIMEOUT_SECONDS
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit


object NetworkProperties {
    const val TIMEOUT_SECONDS = 15L
}

val networkModule = module {

    single { createLoggingInterceptor() }

    single { createStethoInterceptor() }

    single { createHostnameInterceptor(get()) }

    single { createBasicAuthInterceptor(get()) }

    single { createConnectivityInterceptor(androidApplication()) }

    single { createOkHttpClient(get(), get(), get(), get(), get()) }

    single { createWebService<LoginApi>(get()) }

    single { createWebService<StockLocationsProjectsApi>(get()) }

    single { createWebService<ItemTransactionsApi>(get()) }
}

fun createLoggingInterceptor(): HttpLoggingInterceptor {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return httpLoggingInterceptor
}

fun createStethoInterceptor() = StethoInterceptor()

fun createHostnameInterceptor(loginDao: LoginDao) = HostnameInterceptor(loginDao)

fun createBasicAuthInterceptor(loginDao: LoginDao) = BasicAuthInterceptor(loginDao)

fun createConnectivityInterceptor(context: Context) = ConnectivityInterceptor(context)

fun createOkHttpClient(loggingInterceptor: HttpLoggingInterceptor, stethoInterceptor: StethoInterceptor, hostnameInterceptor: HostnameInterceptor, authInterceptor: BasicAuthInterceptor, connectivityInterceptor: ConnectivityInterceptor): OkHttpClient {

    val clientBuilder = OkHttpClient.Builder()
        .connectTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
        .writeTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
        .readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)

    if (BuildConfig.DEBUG) {
        clientBuilder.addInterceptor(loggingInterceptor)
        clientBuilder.addNetworkInterceptor(stethoInterceptor)
    }

    clientBuilder.addInterceptor(hostnameInterceptor)
    clientBuilder.addInterceptor(authInterceptor)
    clientBuilder.addInterceptor(connectivityInterceptor)

    return clientBuilder.build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient): T {
    val serializer = BooleanSerializer()
    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
      //  .registerTypeAdapter(Boolean::class.java, serializer)
      //  .registerTypeAdapter(Boolean::class.javaPrimitiveType, serializer)
        .create()

    return Retrofit.Builder()
        .baseUrl("http://local")
        .client(okHttpClient)
        .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
        //            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(T::class.java)
}

class BooleanSerializer : JsonSerializer<Int?>, JsonDeserializer<Int?> {

    @Throws(JsonParseException::class) override fun deserialize(arg0: JsonElement, arg1: Type?, arg2: JsonDeserializationContext?): Int? {
        val select: Boolean = arg0.asBoolean
        return if (select) 1 else 0
    }

    override fun serialize(src: Int?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return  if(src == 1){
            JsonPrimitive(true)
        } else {
            JsonPrimitive(false)
        }
    }
}
