package com.ionScanApp.di

import com.ionScanApp.MainActivityViewModel
import com.ionScanApp.data.remote.repository.ItemTransactionsRepository
import com.ionScanApp.data.remote.repository.ItemTransactionsRepositoryImpl
import com.ionScanApp.data.remote.repository.LoginRepository
import com.ionScanApp.data.remote.repository.LoginRepositoryImpl
import com.ionScanApp.data.remote.repository.ProjectsLocationsRepository
import com.ionScanApp.data.remote.repository.StockLocationLocationsRepositoryImpl
import com.ionScanApp.ui.common.OriginDestinationViewModel
import com.ionScanApp.ui.common.ProjectsLocationsViewModel
import com.ionScanApp.ui.login.LoginViewModel
import com.ionScanApp.ui.scanned_items.ItemsTransactionsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val ionScanModule = module {

    viewModel { LoginViewModel(get()) }

    viewModel { MainActivityViewModel(get()) }

    viewModel { ProjectsLocationsViewModel(get(), get()) }

    viewModel { ItemsTransactionsViewModel(get(), get(), get(), get()) }

    viewModel { OriginDestinationViewModel(get()) }

    single { LoginRepositoryImpl(get(), get(), get()) as LoginRepository }

    single { StockLocationLocationsRepositoryImpl(get()) as ProjectsLocationsRepository }

    single { ItemTransactionsRepositoryImpl(get()) as ItemTransactionsRepository }
}

val ionScanApp = listOf(ionScanModule)
