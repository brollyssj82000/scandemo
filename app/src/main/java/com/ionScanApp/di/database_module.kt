package com.ionScanApp.di

import android.app.Application
import androidx.room.Room
import com.ionScanApp.data.local.AppIonScanDatabase
import com.ionScanApp.data.local.LoginIonScanDatabase
import com.ionScanApp.data.local.dao.ItemTransactionsDao
import com.ionScanApp.data.local.dao.LoginDao
import com.ionScanApp.data.local.dao.OriginDestinationDao
import com.ionScanApp.data.local.dao.ProjectsLocationsDao
import com.ionScanApp.data.local.dao.SerialNumbersDao
import com.ionScanApp.data.local.dao.UserDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


val databaseModule = module {

    single { createLoginDatabase(androidApplication()) }

    single { createLoginDao(get()) }

    single { createDatabase(androidApplication()) }

    single { createUserDao(get()) }

    single { createProjectsStockLocationsDao(get()) }

    single { createItemTransactionsDao(get()) }

    single { createOriginDestinationDao(get()) }

    single { createSerialNumbersDao(get()) }
}

fun createLoginDatabase(app: Application): LoginIonScanDatabase {
    return Room.databaseBuilder(app, LoginIonScanDatabase::class.java, "ionScan_login.db")
        .fallbackToDestructiveMigration()
        .build()
}

fun createDatabase(app: Application): AppIonScanDatabase {
    return Room.databaseBuilder(app, AppIonScanDatabase::class.java, "ionScan.db")
        .fallbackToDestructiveMigration()
        .build()
}

fun createLoginDao(db: LoginIonScanDatabase): LoginDao = db.loginDao()
fun createUserDao(db: AppIonScanDatabase): UserDao = db.userDao()
fun createProjectsStockLocationsDao(db: AppIonScanDatabase): ProjectsLocationsDao = db.projectsStockLocationsDao()
fun createItemTransactionsDao(db: AppIonScanDatabase): ItemTransactionsDao = db.itemTransactionsDao()
fun createOriginDestinationDao(db: AppIonScanDatabase): OriginDestinationDao = db.originDestinationDao()
fun createSerialNumbersDao(db: AppIonScanDatabase): SerialNumbersDao = db.serialNumbersDao()
