package com.ionScanApp.ui.stock_project

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.OriginDestinationEntity
import com.ionScanApp.databinding.FragmentStockProjectBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.common.CategoryStockProject
import com.ionScanApp.ui.common.DeleteDatabaseProjecstLocationSelectionCache
import com.ionScanApp.ui.common.OriginDestinationViewModel
import com.ionScanApp.ui.common.ProjectsLocationsListActivity
import com.ionScanApp.ui.common.StockProjectDestination
import com.ionScanApp.ui.common.StockProjectOrigin
import org.koin.androidx.viewmodel.ext.android.viewModel


class StockToProjectFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentStockProjectBinding
    private lateinit var navController: NavController
    private var bundle = Bundle()
    private val viewModel: OriginDestinationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.clearDatabaseProjecstLocationSelectionCache(DeleteDatabaseProjecstLocationSelectionCache)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentStockProjectBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        binding.stockProjectSubmitButton.isEnabled = false

        viewModel.getSelectedOriginDestinationFromDb(CategoryStockProject)

        setOnClickListeners()
        setupObservers()
        onBackPressedHandle(navController)
    }

    private fun setupObservers() {
        viewModel.liveDataInsertedItems.observe(viewLifecycleOwner, Observer {

        })

        viewModel.liveDataOriginDestination.observe(viewLifecycleOwner, Observer {
            binding.stockProjectOriginText.text = it.originName
            binding.stockProjectDestinationText.text = it.destinationName
            bundle.putInt("originId", it.originId!!)

            if (it.destinationName.isNullOrBlank()) {
                binding.stockProjectDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom_white)
            } else {
                bundle.putInt("destinationId", it.destinationId!!)
                binding.stockProjectDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
            }

            if (!it.originName.isNullOrBlank()) {
                binding.stockProjectOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
                binding.stockProjectSubmitButton.isEnabled = true
                binding.stockProjectSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
            }
        })
    }

    private fun setOnClickListeners() {
        binding.stockProjectOriginParent.setOnClickListener(this)
        binding.stockProjectDestinationParent.setOnClickListener(this)
        binding.stockProjectSubmitButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.stock_project_origin_parent      -> startActivityForResult(StockProjectOrigin, StockProjectOrigin)
            R.id.stock_project_destination_parent -> startActivityForResult(StockProjectDestination, StockProjectDestination)
            R.id.stock_project_submit_button      -> openTransactions()
        }
    }

    private fun openTransactions() {
        viewModel.addItemToDatabase(OriginDestinationEntity(CategoryStockProject, bundle.getInt("originId"), bundle.getString("originName"), bundle.getInt("destinationId"),
                                                                                  bundle.getString("destinationName")))
        bundle.putInt("category_type", CategoryStockProject)
        navController.navigate(R.id.action_nav_stock_to_project_to_scannedItemsFragment, bundle)
    }

    private fun startActivityForResult(requestCode: Int, origin: Int) {
        startActivityForResult(Intent(context, ProjectsLocationsListActivity::class.java).putExtra("origin", origin), requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activity?.overridePendingTransition(R.anim.slide_in_left, 0);

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                StockProjectOrigin      -> setOriginInfo(StockProjectOrigin, data)
                StockProjectDestination -> setDestinationInfo(StockProjectDestination, data)
            }
        }

        if (!binding.stockProjectOriginText.text.isNullOrEmpty()) { // && !binding.pickingDestinationText.text.isNullOrEmpty()) {
            binding.stockProjectSubmitButton.isEnabled = true
            binding.stockProjectSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
        }
    }

    private fun setOriginInfo(requestCode: Int, data: Intent?) {
        binding.stockProjectOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
        bundle.putInt("originId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("originName", data.extras!!.getString("projectOrLocationName"))
        binding.stockProjectOriginText.text = data.extras!!.getString("projectOrLocationName")
    }

    private fun setDestinationInfo(requestCode: Int, data: Intent?) {
        binding.stockProjectDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
        bundle.putInt("destinationId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("destinationName", data.extras!!.getString("projectOrLocationName"))
        binding.stockProjectDestinationText.text = data.extras!!.getString("projectOrLocationName")
    }
}