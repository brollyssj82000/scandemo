package com.ionScanApp.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ionScanApp.ui.common.DeleteDatabaseProjecstLocationSelectionCache
import com.ionScanApp.ui.common.DeleteDatabaseProjecstLocations
import com.ionScanApp.ui.common.DeleteDatabaseSerialNumbersSource
import com.ionScanApp.ui.common.DeleteDatabaseTransactionsSource
import com.ionScanApp.ui.common.utils.ErrorHandler
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


abstract class BaseViewModel : ViewModel() {

    val disposables = CompositeDisposable()
    var showProgress = MutableLiveData<Boolean>()

    val intError = MutableLiveData<Int>()
    val stringError = MutableLiveData<String>()

    var liveDataClearDatabaseTransactions = MutableLiveData<Boolean>()
    var liveDataClearDatabaseSerialNumbers = MutableLiveData<Boolean>()
    var liveDataClearDatabaseProjectsLocations = MutableLiveData<Boolean>()
    var liveDataClearDatabaseProjectsLocationsSelectionCache = MutableLiveData<Boolean>()

    fun launch(job: () -> Disposable) {
        disposables.add(job())
    }

    fun showProgress() {
        showProgress.postValue(true)
    }

    fun hideProgress() {
        showProgress.postValue(false)
    }

    fun clearDatabase(source: Boolean, functionClearDatabase: () -> Unit) = launch {
        Completable.fromAction { functionClearDatabase() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {
                when (source) {
                    DeleteDatabaseTransactionsSource             -> liveDataClearDatabaseTransactions.value = DeleteDatabaseTransactionsSource
                    DeleteDatabaseSerialNumbersSource            -> liveDataClearDatabaseSerialNumbers.value = DeleteDatabaseSerialNumbersSource
                    DeleteDatabaseProjecstLocations              -> liveDataClearDatabaseProjectsLocations.value = DeleteDatabaseProjecstLocations
                    DeleteDatabaseProjecstLocationSelectionCache -> liveDataClearDatabaseProjectsLocationsSelectionCache.value = DeleteDatabaseProjecstLocationSelectionCache
                }
            }, onError = { ErrorHandler.handle(it, intError, stringError) })
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }


}
