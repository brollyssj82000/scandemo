package com.ionScanApp.ui.stock_stock

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.OriginDestinationEntity
import com.ionScanApp.databinding.FragmentStockStockBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.common.CategoryProjectStock
import com.ionScanApp.ui.common.CategoryStockProject
import com.ionScanApp.ui.common.CategoryStockStock
import com.ionScanApp.ui.common.DeleteDatabaseProjecstLocationSelectionCache
import com.ionScanApp.ui.common.OriginDestinationViewModel
import com.ionScanApp.ui.common.ProjectStockDestination
import com.ionScanApp.ui.common.ProjectStockOrigin
import com.ionScanApp.ui.common.ProjectsLocationsListActivity
import com.ionScanApp.ui.common.StockStockDestination
import com.ionScanApp.ui.common.StockStockOrigin
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class StockToStockFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentStockStockBinding
    private lateinit var navController: NavController
    private var bundle = Bundle()
    private val viewModel: OriginDestinationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.clearDatabaseProjecstLocationSelectionCache(DeleteDatabaseProjecstLocationSelectionCache)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentStockStockBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        binding.stockStockSubmitButton.isEnabled = false

        viewModel.getSelectedOriginDestinationFromDb(CategoryStockStock)

        setOnClickListeners()
        setupObservers()
        onBackPressedHandle(navController)
    }

    private fun setupObservers() {
        viewModel.liveDataInsertedItems.observe(viewLifecycleOwner, Observer {

        })

        viewModel.liveDataOriginDestination.observe(viewLifecycleOwner, Observer {
            binding.stockStockOriginText.text = it.originName
            binding.stockStockDestinationText.text = it.destinationName
            bundle.putInt("originId", it.originId!!)

            if (it.destinationName.isNullOrBlank()) {
                binding.stockStockDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom_white)
            } else {
                bundle.putInt("destinationId", it.destinationId!!)
                binding.stockStockDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
            }

            if (!it.originName.isNullOrBlank()) {
                binding.stockStockOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
                binding.stockStockSubmitButton.isEnabled = true
                binding.stockStockSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
            }
        })
    }

    private fun setOnClickListeners() {
        binding.stockStockOriginParent.setOnClickListener(this)
        binding.stockStockDestinationParent.setOnClickListener(this)
        binding.stockStockSubmitButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.stock_stock_origin_parent      -> startActivityForResult(StockStockOrigin, StockStockOrigin)
            R.id.stock_stock_destination_parent -> startActivityForResult(StockStockDestination, StockStockDestination)
            R.id.stock_stock_submit_button      -> openTransactions()
        }
    }

    private fun openTransactions() {
        viewModel.addItemToDatabase(OriginDestinationEntity(CategoryStockStock, bundle.getInt("originId"), bundle.getString("originName"),
                                                            bundle.getInt("destinationId"), bundle.getString("destinationName")))
        bundle.putInt("category_type", CategoryStockStock)
        navController.navigate(R.id.action_nav_stock_to_stock_to_scannedItemsFragment, bundle)
    }

    private fun startActivityForResult(requestCode: Int, origin: Int) {
        startActivityForResult(Intent(context, ProjectsLocationsListActivity::class.java).putExtra("origin", origin), requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activity?.overridePendingTransition(R.anim.slide_in_left, 0);

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                StockStockOrigin      -> setOriginInfo(StockStockOrigin, data)
                StockStockDestination -> setDestinationInfo(StockStockDestination, data)
            }
        }

        if (!binding.stockStockOriginText.text.isNullOrEmpty()) { // && !binding.pickingDestinationText.text.isNullOrEmpty()) {
            binding.stockStockSubmitButton.isEnabled = true
            binding.stockStockSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
        }
    }

    private fun setOriginInfo(requestCode: Int, data: Intent?) {
        binding.stockStockOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
        bundle.putInt("originId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("originName", data.extras!!.getString("projectOrLocationName"))
        binding.stockStockOriginText.text = data.extras!!.getString("projectOrLocationName")
    }

    private fun setDestinationInfo(requestCode: Int, data: Intent?) {
        binding.stockStockDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
        bundle.putInt("destinationId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("destinationName", data.extras!!.getString("projectOrLocationName"))
        binding.stockStockDestinationText.text = data.extras!!.getString("projectOrLocationName")
    }
}