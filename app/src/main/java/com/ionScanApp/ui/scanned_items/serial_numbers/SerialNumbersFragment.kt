package com.ionScanApp.ui.scanned_items.serial_numbers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.SerialNumbersResponse
import com.ionScanApp.databinding.FragmentSerialNumbersBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.common.DeleteDatabaseSerialNumbersSource
import com.ionScanApp.ui.common.ItemClickListener
import com.ionScanApp.ui.scanned_items.ItemsTransactionsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SerialNumbersFragment : BaseFragment(), View.OnClickListener, SerialNumbersListListener {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentSerialNumbersBinding
    private val viewModel: ItemsTransactionsViewModel by viewModel()

    private lateinit var serialNumbersAdapter: SerialNumbersAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSerialNumbersBinding.inflate(layoutInflater)
      //  if(arguments?.containsKey("openedItemId")!!){

       // }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = Navigation.findNavController(view)

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                navController.popBackStack()
            }
        })

        setupRecycler()
        setupObservers()
        setOnClickListeners()
        viewModel.getAllItemTransactionsDB(true)
    }

    override fun onBackPressedHandle(navController: NavController) {
        requireActivity().onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // navController.navigate(R.id.action_nav_stock_to_project_to_nav_home)
                navController.popBackStack()
            }
        })
    }

    private fun setupObservers() {

        viewModel.liveDataSerialNumbersResponse.observe(viewLifecycleOwner, Observer {
            serialNumbersAdapter.updateList(it as ArrayList<SerialNumbersResponse>)
        })

        viewModel.liveDataBookingResponse.observe(viewLifecycleOwner, Observer {
            navigateToBookingConfirmationScreen()
        })

        viewModel.stringError.observe(viewLifecycleOwner, Observer {
            showCustomErrorDialog(context, it)
        })
    }

    private fun setOnClickListeners() {
        binding.bookButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bookButton -> bookItemsButtonValidate()
        }
    }

    private fun bookItemsButtonValidate() {
        viewModel.deleteUnselectedSerialNumbers()
    }

    private fun navigateToBookingConfirmationScreen() {
        navController.navigate(R.id.action_serialNumbersFragment_to_bookedItemConfirmationFragment)
    }

    private fun setupRecycler() {
        serialNumbersAdapter = SerialNumbersAdapter(arrayListOf(), this)
        binding.serialNumbersRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()
            // setItemViewCacheSize(10)
            if (!serialNumbersAdapter.hasObservers()) {
                adapter?.setHasStableIds(true)
            }
            adapter = serialNumbersAdapter
        }
    }

    override fun onSerialNumberUpdate(serialNumber: String, id: Int) {
        viewModel.updateSerialNumber(serialNumber, id)
    }

    override fun onSerialNumberCheckedBoxClick(id: Int, isSelected: Int) {
        viewModel.updateSerialNumberIsSelected(id, isSelected)
    }

    override fun onDestroy() {
        viewModel.clearDatabaseSerialNumbersItems(DeleteDatabaseSerialNumbersSource)
        viewModel.clearDatabaseSerialNumbers(DeleteDatabaseSerialNumbersSource)
        super.onDestroy()
    }
}
