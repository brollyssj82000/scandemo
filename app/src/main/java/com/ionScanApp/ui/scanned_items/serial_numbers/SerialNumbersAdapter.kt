package com.ionScanApp.ui.scanned_items.serial_numbers

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.SerialNumbersResponse
import com.ionScanApp.data.local.entity.SerialNumbersResponseItem
import com.ionScanApp.ui.common.ItemClickListener
import com.ionScanApp.ui.common.utils.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.serial_numbers_parent_list_item.*

class SerialNumbersAdapter(private var serialNumbersResponse: ArrayList<SerialNumbersResponse>, private val serialNumbersListListener: SerialNumbersListListener) : RecyclerView.Adapter<SerialNumbersAdapter.SerialNumbersViewHolder>() {

    fun updateList(newSerialNumbersListItem: ArrayList<SerialNumbersResponse>) {
        serialNumbersResponse.clear()
        serialNumbersResponse.addAll(newSerialNumbersListItem)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return serialNumbersResponse.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerialNumbersViewHolder = SerialNumbersViewHolder(parent.inflate(R.layout.serial_numbers_parent_list_item))


    override fun onBindViewHolder(holder: SerialNumbersViewHolder, position: Int) {
        holder.bind(serialNumbersResponse[position])
    }

    inner class SerialNumbersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        fun bind(serialNumbersResponse: SerialNumbersResponse) {
             //if (serialNumbersResponse.itemSerialNumber!!) {
                    serialNumberItemName.text = String.format(itemView.context.resources.getString(R.string.please_enter_serial_number_s_for_item_itemname), serialNumbersResponse.itemName)
                    setupRecycler(serialNumbersResponse.serialNumbers)
            //}
        }

        private fun setupRecycler(serialNumbersListItems: List<SerialNumbersResponseItem>?) {
            val serialNumbersChildAdapter = SerialNumbersChildAdapter(serialNumbersListItems!!, serialNumbersListListener)
            serialNumbersChildRecyclerVIew.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                itemAnimator = DefaultItemAnimator()
                // setItemViewCacheSize(10)
                /*if (!serialNumbersChildAdapter.hasObservers()) {
                    adapter?.setHasStableIds(true)
                }*/
                adapter = serialNumbersChildAdapter
            }
        }
    }
}