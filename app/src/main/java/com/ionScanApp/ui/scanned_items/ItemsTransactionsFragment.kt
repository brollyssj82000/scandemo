package com.ionScanApp.ui.scanned_items

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.databinding.FragmentScanItemsListBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.common.CategoryProjectProject
import com.ionScanApp.ui.common.CategoryProjectStock
import com.ionScanApp.ui.common.CategoryProjectStockPicking
import com.ionScanApp.ui.common.CategoryStockProject
import com.ionScanApp.ui.common.CategoryStockStock
import com.ionScanApp.ui.common.DeleteDatabaseTransactionsSource
import com.ionScanApp.ui.common.EditTextChangeListener
import com.ionScanApp.ui.common.ItemClickListener
import com.ionScanApp.ui.common.ItemTransactionClickListener
import com.ionScanApp.ui.common.PaginationScrollListener
import com.ionScanApp.ui.common.Project
import com.ionScanApp.ui.common.Stock
import com.ionScanApp.ui.common.utils.visible
import com.ionScanApp.ui.scanning.ScanningActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.Executors


class ItemsTransactionsFragment : BaseFragment(), ItemTransactionClickListener, EditTextChangeListener, View.OnClickListener {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentScanItemsListBinding
    private val viewModel: ItemsTransactionsViewModel by viewModel()

    private var originId: Int? = null
    private var destinationId: Int? = null
    private var category_type: Int? = null

    private var isLoadingMore = true
    private var hasLoadedAllItems = false

    private var FIRSTPAGE = 1
    private lateinit var paginationListener: PaginationScrollListener

    private lateinit var itemTransactionsAdapter: PagedListAdapter<ItemTransactionsResponse, ItemTransactionsAdapter.ItemTransactionsViewHolder>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentScanItemsListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        viewModel.getSelectedOriginDestinationFromDb()
        setHasOptionsMenu(true)

     /*   originId = arguments?.getInt("originId")
        destinationId = arguments?.getInt("destinationId")
        category_type = arguments?.getInt("category_type")
        Log.d("InstanceStateOriginId", "originId: " + originId)*/

       /* setupRecycler()*/
        itemTransactionsAdapter = ItemTransactionsAdapter(category_type, this, this)
        setupObservers()
        setOnClickListeners()
        setupRecycler()

        binding.swipeRefreshItemTransactions.setOnRefreshListener {
            binding.swipeRefreshItemTransactions.isRefreshing = true
            viewModel.clearDatabase(DeleteDatabaseTransactionsSource)
        }
    }

/*    override fun onResume() {
        super.onResume()
        viewModel.clearDatabase(DeleteDatabaseTransactionsSource)
    }*/

    private fun getItemTransations(category_type: Int?) {
        when (category_type) {
            CategoryProjectStock        -> initBookAllButton(Project, originId, Stock, destinationId, true)
            CategoryStockProject        -> initBookAllButton(Stock, originId, Project, destinationId, true)
            CategoryProjectProject      -> initBookAllButton(Project, originId, Project, destinationId, true)
            CategoryStockStock          -> initBookAllButton(Stock, originId, Stock, destinationId, true)
            CategoryProjectStockPicking -> initBookAllButton(Project, originId, Stock, destinationId, false)
        }
    }

    private fun initBookAllButton(categoryOrigin: Int, originId: Int?, categoryDestination: Int, destinationId: Int?, setSerialNumbersButtonVisibility: Boolean) {
        binding.setItemsSerialNumbersButton.visible = setSerialNumbersButtonVisibility
        initItemTransactions(categoryOrigin, originId, categoryDestination, destinationId)
    }

    private fun initItemTransactions(categoryOrigin: Int, originId: Int?, categoryDestination: Int, destinationId: Int?) {
        viewModel.getItemTransactionsApi(categoryOrigin, originId, categoryDestination, destinationId, 1, 20)
    }

    private fun setOnClickListeners() {
        binding.noDataItemTransactions.noDataBackToMenuButton.setOnClickListener(this)
        binding.setItemsSerialNumbersButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.no_data_back_to_menu_button     -> navController.popBackStack(R.id.nav_home, true)
            R.id.set_items_serial_numbers_button -> navController.navigate(R.id.action_itemsTransactionsFragment_to_serialNumbersFragment)
        }
    }

    private fun setupObservers() {

        viewModel.itemPagedList.observe(viewLifecycleOwner, Observer {
            itemTransactionsAdapter.submitList(it)
            binding.swipeRefreshItemTransactions.isRefreshing = false
        })

        viewModel.liveDataClearDatabaseTransactions.observe(viewLifecycleOwner, Observer {
          //  getItemTransations(category_type)
        })

        viewModel.liveDataOriginDestinationForTransactions.observe(viewLifecycleOwner, Observer {
            originId = it.originId
            destinationId = it.destinationId
            category_type = it.category_type_id

            getItemTransations(category_type)
           // itemTransactionsAdapter = ItemTransactionsAdapter(category_type, this, this)
          //  setupRecycler()
        })

        viewModel.liveDataDatabaseEntryRemoved.observe(viewLifecycleOwner, Observer {
            itemTransactionsAdapter.currentList?.dataSource?.invalidate()
        })

        viewModel.liveDataItemTransactionsList.observe(viewLifecycleOwner, Observer {
            when {
                it.isNullOrEmpty() -> {
                    unbindPagination()
                    binding.noDataItemTransactions.root.visibility = View.VISIBLE
                    binding.setItemsSerialNumbersButton.visibility = View.GONE
                }
                it.size < 10       -> {
                    unbindPagination()
                }
                else               -> {
                    hasLoadedAllItems = false
                    isLoadingMore = true
                }
            }
        })

        viewModel.showProgress.observe(viewLifecycleOwner, Observer {
            binding.progressBarItemTransactions.visible = it!!
        })

        viewModel.stringError.observe(viewLifecycleOwner, Observer {
            binding.setItemsSerialNumbersButton.visible = false
            showCustomErrorDialog(context, it)
        })
    }

    private fun unbindPagination() {
        hasLoadedAllItems = true
        isLoadingMore = false
        binding.scannedItemsListRecyclerView.removeOnScrollListener(paginationListener)
    }

    private fun setupRecycler() {
        binding.scannedItemsListRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()
            if (!itemTransactionsAdapter.hasObservers()) {
                adapter?.setHasStableIds(true)
            }
            adapter = itemTransactionsAdapter
        }
        setupRecylerPagination()

        swipeRemoveRecyclerViewItem()

         when {
             category_type != CategoryProjectStockPicking -> swipeRemoveRecyclerViewItem()
         }
    }

    private fun swipeRemoveRecyclerViewItem() {
        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.removeItemFromDatabase(viewHolder.adapterPosition, (viewHolder as ItemTransactionsAdapter.ItemTransactionsViewHolder).item.id)
               // itemTransactionsAdapter.notifyItemRemoved(viewHolder.adapterPosition)
            }
        }).attachToRecyclerView(binding.scannedItemsListRecyclerView)
    }

    private fun setupRecylerPagination() {
        paginationListener = object : PaginationScrollListener(binding.scannedItemsListRecyclerView.layoutManager as LinearLayoutManager, 20) {
            override fun loadMoreItems() {
                isLoadingMore = true
                viewModel.getItemTransactionsApi(Project, originId!!, Stock, destinationId!!, ++FIRSTPAGE, 20)
                getItemTransations(category_type)

            }

            override fun isLastPage(): Boolean {
                return hasLoadedAllItems
            }

            override fun isLoading(): Boolean {
                return isLoadingMore
            }
        }
        binding.scannedItemsListRecyclerView.addOnScrollListener(paginationListener)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.scan_item -> return true//openCameraForScanningBarcode() //return true
        }
        return false
    }

    /*private fun openCameraForScanningBarcode(){
        val intent = Intent(context, ScanningActivity::class.java)
        startActivity(intent)
    }*/

    override fun onItemClickOpenItemTransaction(id: Int) {
        Log.d("ClickHappened", "Click")
        val bundle = bundleOf("openedItemId" to id)
        navController.navigate(R.id.action_scannedItemsFragment_to_scannedItemDetailsFragment, bundle)
    }

    override fun onToBookEditTextChange(toBookValue: Int?, itemId: Int, adapterPosition: Int) {
        viewModel.updateToBookItemTransaction(toBookValue, itemId, true)
    }

    override fun onToBookEditTextChangeError(toBookValue: Int?) {
        binding.setItemsSerialNumbersButton.visibility = View.GONE
        showErrorDialog(context, "Wrong amount", "The entered amount must not be lower than 1 or higher than $toBookValue.")
    }

    override fun onToBookEditTextChangeEmptyError(itemName: String?, adapterPosition: Int) {
        binding.setItemsSerialNumbersButton.visibility = View.GONE
        showErrorDialog(context, "Wrong amount", "Item $itemName at position ${adapterPosition + 1} does not have any number value set.")
    }

    override fun onDestroy() {
        viewModel.clearDatabase(DeleteDatabaseTransactionsSource)
        super.onDestroy()
    }
}