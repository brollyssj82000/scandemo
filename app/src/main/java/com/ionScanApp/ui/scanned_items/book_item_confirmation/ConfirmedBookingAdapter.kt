package com.ionScanApp.ui.scanned_items.book_item_confirmation

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.ui.common.utils.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.confirmed_booking_list_item.*

class ConfirmedBookingAdapter(private var bookeItemsList: ArrayList<ItemTransactionsResponse>): RecyclerView.Adapter<ConfirmedBookingAdapter.BookedItemConfirmationViewHolder>() {

    fun updateList(newBookeItemsList: ArrayList<ItemTransactionsResponse>) {
        bookeItemsList.clear()
        bookeItemsList.addAll(newBookeItemsList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return bookeItemsList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookedItemConfirmationViewHolder = BookedItemConfirmationViewHolder(parent.inflate(R.layout.confirmed_booking_list_item))


    override fun onBindViewHolder(holder: BookedItemConfirmationViewHolder, position: Int) {
        holder.bind(bookeItemsList[position])
    }

    inner class BookedItemConfirmationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        fun bind(item: ItemTransactionsResponse) {
            done_booking_itemid.text = item.id.toString()
            done_booking_item_name.text = item.itemName
        }
    }
}