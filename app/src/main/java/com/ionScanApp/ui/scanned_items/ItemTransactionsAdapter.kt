package com.ionScanApp.ui.scanned_items

import android.view.View
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.ui.common.CategoryProjectStockPicking
import com.ionScanApp.ui.common.EditTextChangeListener
import com.ionScanApp.ui.common.ItemClickListener
import com.ionScanApp.ui.common.ItemTransactionClickListener
import com.ionScanApp.ui.common.utils.getColor
import com.ionScanApp.ui.common.utils.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.scanned_list_item.*

class ItemTransactionsAdapter(val category_type: Int?, val itemClickListener: ItemTransactionClickListener, val editTextChangeListener: EditTextChangeListener) : PagedListAdapter<ItemTransactionsResponse, ItemTransactionsAdapter.ItemTransactionsViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ItemTransactionsResponse>() {
            override fun areItemsTheSame(oldItem: ItemTransactionsResponse, newItem: ItemTransactionsResponse): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ItemTransactionsResponse, newItem: ItemTransactionsResponse): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemTransactionsViewHolder = ItemTransactionsViewHolder(parent.inflate(R.layout.scanned_list_item))

    override fun onBindViewHolder(holder: ItemTransactionsViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    inner class ItemTransactionsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        lateinit var item: ItemTransactionsResponse

        fun bind(item: ItemTransactionsResponse) {
            this.item = item

            scanned_item_id.text = item.itemItemId
            scanned_item_name.text = item.itemName
            remaining_value.text = item.remainingAmount.toString()
            in_stock_value.text = item.quantityInStock.toString()
            booked_value.text = item.bookedAmount.toString()
            required_value.text = item.requiredAmount.toString()
            to_book_value.setText(item.toBookAmount.toString())

            when (category_type) {
                CategoryProjectStockPicking -> to_book_value.isEnabled = false
            }

            if (item.remainingAmount != 0) {
                remaining_value.setBackgroundColor(getColor("#ffffff", R.color.white))
                remaining.setBackgroundColor(getColor("#ffffff", R.color.white))
            }

            if (item.quantityInStock != 0) {
                in_stock_value.setBackgroundColor(getColor("#ffffff", R.color.white))
                in_stock.setBackgroundColor(getColor("#ffffff", R.color.white))
            }

            if (item.bookedAmount != 0) {
                booked_value.setBackgroundColor(getColor("#ffffff", R.color.white))
                booked.setBackgroundColor(getColor("#ffffff", R.color.white))
            }

            if (item.requiredAmount != 0) {
                required_value.setBackgroundColor(getColor("#ffffff", R.color.white))
                required.setBackgroundColor(getColor("#ffffff", R.color.white))
            }

            /*if (item.toBookAmount != 0) {
                to_book_value.setBackgroundColor(getColor("#ffffff", R.color.white))
                to_book.setBackgroundColor(getColor("#ffffff", R.color.white))
            }*/

            clickGroup.setOnClickListener {
                itemClickListener.onItemClickOpenItemTransaction(item.id)
            }

            to_book_value.setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    if (to_book_value.text.trim().isNotBlank() and to_book_value.text.trim().isDigitsOnly()) {
                        if (to_book_value.text.trim().toString().toInt() in 1..item.toBookAmount!!) {
                            editTextChangeListener.onToBookEditTextChange(to_book_value.text.trim().toString().toInt(), item.id, adapterPosition)
                        } else {
                            editTextChangeListener.onToBookEditTextChangeError(item.toBookAmount)
                            to_book_value.setText(item.toBookAmount.toString())
                        }
                    } else {
                        editTextChangeListener.onToBookEditTextChangeEmptyError(item.itemName, adapterPosition)
                    }
                }
            }
        }
    }
}