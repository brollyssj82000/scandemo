package com.ionScanApp.ui.scanned_items

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ionScanApp.data.local.dao.ItemTransactionsDao
import com.ionScanApp.data.local.dao.OriginDestinationDao
import com.ionScanApp.data.local.dao.SerialNumbersDao
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.data.local.entity.OriginDestinationEntity
import com.ionScanApp.data.local.entity.SerialNumbersResponse
import com.ionScanApp.data.local.entity.SerialNumbersResponseItem
import com.ionScanApp.data.local.entity.TransactionWithSerialNumbers
import com.ionScanApp.data.remote.repository.ItemTransactionsRepository
import com.ionScanApp.ui.BaseViewModel
import com.ionScanApp.ui.common.utils.ErrorHandler
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class ItemsTransactionsViewModel(private val itemTransactionsDao: ItemTransactionsDao, private val serialNumbersDao: SerialNumbersDao, private val originDestinationDao: OriginDestinationDao, private val itemTransactionsRepository: ItemTransactionsRepository) : BaseViewModel() {

    var liveDataItemTransactionsList = MutableLiveData<List<ItemTransactionsResponse>>()
    var liveDataSelectedItemTransactionDetails = MutableLiveData<List<ItemTransactionsResponse>>()
    var liveDataDatabaseEntryRemoved = MutableLiveData<Int>()
    var liveDataUpdateItemTransactionToBookValue = MutableLiveData<Boolean>()
    var liveDataSerialNumbersResponse = MutableLiveData<List<SerialNumbersResponse>>()
    var liveDataBookingResponse = MutableLiveData<List<ItemTransactionsResponse>>()
    var liveDataItemTransactionsWithoutSerialNumbers = MutableLiveData<List<ItemTransactionsResponse>>()
    var liveDataOriginDestinationForTransactions = MutableLiveData<OriginDestinationEntity>()

    val itemPagedList: LiveData<PagedList<ItemTransactionsResponse>> by lazy {
        val dataSourceFactory = itemTransactionsDao.getItemTransactionsPaginated()
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setInitialLoadSizeHint(20)
            .build()
        LivePagedListBuilder(dataSourceFactory, config).build()
    }

    fun getSelectedOriginDestinationFromDb() = launch {
        originDestinationDao.getSelectedOriginDestination()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onSuccess = {
                liveDataOriginDestinationForTransactions.value = it
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun getAllItemTransactionsDB(getSerialNumbers: Boolean) = launch {
        itemTransactionsDao.getItemTransactionsDB()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .onBackpressureBuffer()
            .subscribeBy(onNext = {
                if (getSerialNumbers) {
                    getSerialNumbersFromApi(it)
                } else {
                    liveDataItemTransactionsWithoutSerialNumbers.value = it
                }
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    private fun getSerialNumbersFromApi(serialNumbers: List<ItemTransactionsResponse>) = launch {
        itemTransactionsRepository.getSerialNumbers(serialNumbers)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .onBackpressureBuffer()
            .subscribeBy(onNext = {
                insertSerialNumbers(it)
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    private fun insertSerialNumbers(serialNumbersResponse: List<SerialNumbersResponse>) = launch {
        Completable.fromCallable { serialNumbersDao.insertSerialNumbersResponse(serialNumbersResponse) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {

                for(i in serialNumbersResponse){
                    insertSerialNumbersItems(i.serialNumbers)
                }
                liveDataSerialNumbersResponse.value = serialNumbersResponse

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    private fun insertSerialNumbersItems(serialNumbersResponseItem: List<SerialNumbersResponseItem>?) = launch {
        Completable.fromCallable { serialNumbersDao.insertSerialNumbersItems(serialNumbersResponseItem) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun updateSerialNumber(serialNumber: String, id: Int) = launch {
        Completable.fromCallable { serialNumbersDao.updateSerialNumber(serialNumber, id) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun updateSerialNumberIsSelected(id: Int, isSelected: Int) = launch {
        Completable.fromCallable { serialNumbersDao.updateSerialNumberIsSelected(id, isSelected) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun deleteUnselectedSerialNumbers() = launch {
        Completable.fromCallable { serialNumbersDao.deleteUnselectedSerialNumbers(0) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {
                bookItem()
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    private fun bookItem() = launch {
        serialNumbersDao.getTransactionWIthSerialNumbers()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onNext = {
                bookItemApi(it)

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    private fun bookItemApi(serialNumbersReponseForBooking: List<TransactionWithSerialNumbers>) = launch {
        itemTransactionsRepository.bookItem(serialNumbersReponseForBooking)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onNext = {
                liveDataBookingResponse.value = it

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun getItemTransactionsApi(from: Int, source: Int?, to: Int, destination: Int?, page: Int, pageSize: Int) = launch {
        itemTransactionsRepository.getItemTransactions(from, source, to, destination, page, pageSize)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onNext = {
                addItemsToDatabase(it)
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun getItemTransactionFromDb(id: Int) = launch {
        itemTransactionsDao.getItemTransaction(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onSuccess = {

                liveDataSelectedItemTransactionDetails.value = it

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    private fun addItemsToDatabase(items: List<ItemTransactionsResponse>) = launch {
        Completable.fromCallable { itemTransactionsDao.clearDatabaseThenInsertNewItems(items) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {
                liveDataItemTransactionsList.value = items
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun updateToBookItemTransaction(toBookValue: Int?, id: Int?, multipleUpdates: Boolean) = launch {
        Completable.fromCallable { itemTransactionsDao.updateToBookItemTransaction(toBookValue, id) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {
                if(!multipleUpdates){
                    deleteTransactionsThatDontHaveSelectedID(id)
                    //liveDataUpdateItemTransactionToBookValue.value = true
                }

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    private fun deleteTransactionsThatDontHaveSelectedID(id: Int?) = launch {
        Completable.fromCallable { itemTransactionsDao.deleteDatabaseEntryThatDontMatchID(id) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {
                liveDataUpdateItemTransactionToBookValue.value = true
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun removeItemFromDatabase(adapterPosition: Int, itemId: Int) = launch {
        Completable.fromCallable { itemTransactionsDao.deleteDatabaseEntry(itemId) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {
                liveDataDatabaseEntryRemoved.value = adapterPosition
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun clearDatabase(source: Boolean) = clearDatabase(source) {
        itemTransactionsDao.deleteDatabaseEntries()
    }

    fun clearDatabaseSerialNumbersItems(source: Boolean) = clearDatabase(source) {
        serialNumbersDao.deleteSerialNumberItems()
    }

    fun clearDatabaseSerialNumbers(source: Boolean) = clearDatabase(source) {
        serialNumbersDao.deleteSerialNumberResponse()
    }
}