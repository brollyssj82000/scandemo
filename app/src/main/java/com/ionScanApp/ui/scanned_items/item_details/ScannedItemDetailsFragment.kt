package com.ionScanApp.ui.scanned_items.item_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.databinding.FragmentScannedItemDetailsBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.common.utils.visible
import com.ionScanApp.ui.scanned_items.ItemsTransactionsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ScannedItemDetailsFragment : BaseFragment(), View.OnClickListener {

    private var openedItemId: Int? = null
    private lateinit var binding: FragmentScannedItemDetailsBinding
    private lateinit var navController: NavController

    private val viewModel: ItemsTransactionsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        openedItemId = arguments?.getInt("openedItemId")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentScannedItemDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setupObservers()
        setOnClickListeners()
        viewModel.getItemTransactionFromDb(openedItemId!!)
    }

    private fun setupObservers() {

        viewModel.liveDataSelectedItemTransactionDetails.observe(viewLifecycleOwner, Observer {
            setViewValues(it)
        })

        viewModel.liveDataUpdateItemTransactionToBookValue.observe(viewLifecycleOwner, Observer {
            navController.navigate(R.id.action_scannedItemDetailsFragment_to_serialNumbersFragment, bundleOf("openedItemId" to openedItemId))
        })

        viewModel.showProgress.observe(viewLifecycleOwner, Observer {
            binding.progressBarItemDetails.visible = it!!
        })

        viewModel.stringError.observe(viewLifecycleOwner, Observer {
            showCustomErrorDialog(context, it)
        })
    }

    private fun setViewValues(itemTransactionResponse: List<ItemTransactionsResponse>){
        binding.itemDetailsId.text = itemTransactionResponse[0].itemItemId.toString()
        binding.itemDetailsName.text = itemTransactionResponse[0].itemName
        binding.detailsRequiredValue.text = itemTransactionResponse[0].requiredAmount.toString()
        binding.detailsRemainingValue.text = itemTransactionResponse[0].remainingAmount.toString()
        binding.detailsInStockValue.text = itemTransactionResponse[0].quantityInStock.toString()
        binding.detailsBookedValue.text = itemTransactionResponse[0].bookedAmount.toString()
        binding.detailsToBookValue.setText(itemTransactionResponse[0].toBookAmount.toString())
    }

    private fun updateToBookValue(){
        viewModel.updateToBookItemTransaction(binding.detailsToBookValue.text.trim().toString().toInt(), openedItemId, false)
    }

    private fun setOnClickListeners() {
        binding.setItemSerialNumbersButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.set_item_serial_numbers_button -> openSerialNumbersScreenOrError()
        }
    }

    private fun openSerialNumbersScreenOrError(){
        if (binding.detailsToBookValue.text?.trim()?.isDigitsOnly()!! and (binding.detailsToBookValue.text?.trim().toString().toInt() in 1..viewModel.liveDataSelectedItemTransactionDetails.value?.get(0)?.toBookAmount!!)) {
            updateToBookValue()
        } else {
            showErrorDialog(context, "Wrong amount", "The entered amount must not be lower than 1 or higher than ${viewModel.liveDataSelectedItemTransactionDetails.value?.get(0)?.toBookAmount!!}")
        }
    }
}