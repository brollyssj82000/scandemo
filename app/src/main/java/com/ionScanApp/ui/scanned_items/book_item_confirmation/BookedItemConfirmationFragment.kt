package com.ionScanApp.ui.scanned_items.book_item_confirmation

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.ItemTransactionsResponse
import com.ionScanApp.data.local.entity.SerialNumbersResponse
import com.ionScanApp.databinding.FragmentDoneBookingConfirmationBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.scanned_items.ItemsTransactionsViewModel
import com.ionScanApp.ui.scanned_items.serial_numbers.SerialNumbersAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class BookedItemConfirmationFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentDoneBookingConfirmationBinding
    private lateinit var navController: NavController

    private var confirmedItemId: String? = null
    private var confirmedItemName: String? = null

    private var originId: Int? = null
    private var destinationId: Int? = null

    private val viewModel: ItemsTransactionsViewModel by viewModel()

    private lateinit var confirmedBookingAdapter: ConfirmedBookingAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       /* confirmedItemId = arguments?.getString("confirmedItemId")
        confirmedItemName = arguments?.getString("confirmedItemName")*/

        originId = arguments?.getInt("originId")
        destinationId = arguments?.getInt("destinationId")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDoneBookingConfirmationBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        binding.bookAnotherItem.setOnClickListener(this)

        setupRecycler()
        setupObservers()
        viewModel.getAllItemTransactionsDB(false)
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                navController.navigate(R.id.action_bookedItemConfirmationFragment_to_scannedItemsFragment)
            }
        })
    }

    private fun setupObservers() {

        viewModel.liveDataItemTransactionsWithoutSerialNumbers.observe(viewLifecycleOwner, Observer {

            confirmedBookingAdapter.updateList(it as ArrayList<ItemTransactionsResponse>)
        })


        viewModel.stringError.observe(viewLifecycleOwner, Observer {
            showCustomErrorDialog(context, it)
        })
    }

    private fun setupRecycler() {
        confirmedBookingAdapter = ConfirmedBookingAdapter(arrayListOf())
        binding.confirmedBookedItemsLIst.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()
            // setItemViewCacheSize(10)
            if (!confirmedBookingAdapter.hasObservers()) {
                adapter?.setHasStableIds(true)
            }
            adapter = confirmedBookingAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
           R.id.book_another_item -> navController.navigate(R.id.action_bookedItemConfirmationFragment_to_scannedItemsFragment, bundleOf("originId" to originId,
                                                                                                                                         "destinationId"  to destinationId))
        }
    }

    override fun onBackPressedHandle(navController: NavController) {
        requireActivity().onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
               // errorDialog?.dismiss()
                // navController.navigate(R.id.action_nav_stock_to_project_to_nav_home)
                navController.navigate(R.id.action_bookedItemConfirmationFragment_to_scannedItemsFragment)
            }
        })
    }
}