package com.ionScanApp.ui.scanned_items.serial_numbers

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.SerialNumbersResponseItem
import com.ionScanApp.ui.common.ItemClickListener
import com.ionScanApp.ui.common.utils.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.serial_numbers_child_list_item.*


class SerialNumbersChildAdapter(private var serialNumbersListItem: List<SerialNumbersResponseItem>, val serialNumbersListListener: SerialNumbersListListener) : RecyclerView.Adapter<SerialNumbersChildAdapter.SerialNumbersChildViewHolder>() {

    override fun getItemCount(): Int {
        return serialNumbersListItem.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerialNumbersChildViewHolder = SerialNumbersChildViewHolder(parent.inflate(R.layout.serial_numbers_child_list_item))


    override fun onBindViewHolder(holder: SerialNumbersChildViewHolder, position: Int) {
        holder.bind(serialNumbersListItem[position])
    }

    inner class SerialNumbersChildViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        fun bind(item: SerialNumbersResponseItem) {

            serialNumberCheckBox.isChecked = item.selected //&& item.serialNumber.trim().isNotBlank()

            // serialNumberEditText.isCursorVisible = true

            if (item.serialNumber.trim().isBlank()) {
                serialNumberEditText.setText("")
                serialNumberTextInputLayout.error = String.format(itemView.context.resources.getString(R.string.please_enter_serial_number))
                serialNumberTextInputLayout.isErrorEnabled = true
            } else {
                serialNumberTextInputLayout.error = ""
                serialNumberTextInputLayout.isErrorEnabled = false
                serialNumberEditText.isEnabled = false
                serialNumberEditText.setText(item.serialNumber)
            }

            serialNumberCheckBox.setOnCheckedChangeListener { _, _ ->
                if (serialNumberCheckBox.isChecked) {
                    serialNumbersListListener.onSerialNumberCheckedBoxClick(item.uniqueId, 1)
                } else {
                    serialNumbersListListener.onSerialNumberCheckedBoxClick(item.uniqueId, 0)
                }
            }

            serialNumberEditText.setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    if (serialNumberEditText.text?.trim()!!.isNotBlank()) {
                        serialNumbersListListener.onSerialNumberUpdate(serialNumberEditText.text?.trim().toString(), item.uniqueId)
                        serialNumberTextInputLayout.error = ""
                        serialNumberTextInputLayout.isErrorEnabled = false

                    } else {
                        serialNumberTextInputLayout.error = String.format(itemView.context.resources.getString(R.string.please_enter_serial_number))
                        serialNumberTextInputLayout.isErrorEnabled = true
                    }
                }
            }

            serialNumberEditText.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {
                    if (s.trim().isBlank()) {
                        serialNumberTextInputLayout.error = String.format(itemView.context.resources.getString(R.string.please_enter_serial_number))
                        serialNumberTextInputLayout.isErrorEnabled = true
                    } else {
                        serialNumberEditText.requestFocus()
                        serialNumberTextInputLayout.error = ""
                        serialNumberTextInputLayout.isErrorEnabled = false
                        //itemClickListener.onItemClick(adapterPosition, item.id, s.trim().toString())
                    }
                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                }
            })

            /*if(serialNumberEditText.text?.trim().toString().isNotBlank()) {
                itemClickListener.onItemClick(adapterPosition, item.id, serialNumberEditText.text.toString())
            }*/
        }
    }
}