package com.ionScanApp.ui.scanned_items.serial_numbers

interface SerialNumbersListListener {

    fun onSerialNumberUpdate(serialNumber: String, id: Int)
    fun onSerialNumberCheckedBoxClick(id: Int, isSelected: Int)
}