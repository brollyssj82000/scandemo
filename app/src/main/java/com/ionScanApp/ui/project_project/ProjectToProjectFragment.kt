package com.ionScanApp.ui.project_project

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.OriginDestinationEntity
import com.ionScanApp.databinding.FragmentProjectProjectBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.common.CategoryProjectProject
import com.ionScanApp.ui.common.CategoryProjectStock
import com.ionScanApp.ui.common.DeleteDatabaseProjecstLocationSelectionCache
import com.ionScanApp.ui.common.OriginDestinationViewModel
import com.ionScanApp.ui.common.ProjectProjectDestination
import com.ionScanApp.ui.common.ProjectProjectOrigin
import com.ionScanApp.ui.common.ProjectStockDestination
import com.ionScanApp.ui.common.ProjectStockOrigin
import com.ionScanApp.ui.common.ProjectsLocationsListActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProjectToProjectFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentProjectProjectBinding
    private lateinit var navController: NavController
    private var bundle = Bundle()
    private val viewModel: OriginDestinationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.clearDatabaseProjecstLocationSelectionCache(DeleteDatabaseProjecstLocationSelectionCache)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentProjectProjectBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        binding.projectProjectSubmitButton.isEnabled = false

        viewModel.getSelectedOriginDestinationFromDb(CategoryProjectProject)

        setOnClickListeners()
        setupObservers()
        onBackPressedHandle(navController)
    }

    private fun setupObservers() {
        viewModel.liveDataInsertedItems.observe(viewLifecycleOwner, Observer {

        })

        viewModel.liveDataOriginDestination.observe(viewLifecycleOwner, Observer {
            binding.projectProjectOriginText.text = it.originName
            binding.projectProjectDestinationText.text = it.destinationName
            bundle.putInt("originId", it.originId!!)

            if (it.destinationName.isNullOrBlank()) {
                binding.projectProjectDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom_white)
            } else {
                bundle.putInt("destinationId", it.destinationId!!)
                binding.projectProjectDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
            }

            if (!it.originName.isNullOrBlank()) {
                binding.projectProjectOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
                binding.projectProjectSubmitButton.isEnabled = true
                binding.projectProjectSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
            }
        })
    }

    private fun setOnClickListeners() {
        binding.projectProjectOriginParent.setOnClickListener(this)
        binding.projectProjectDestinationParent.setOnClickListener(this)
        binding.projectProjectSubmitButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.project_project_origin_parent  -> startActivityForResult(ProjectProjectOrigin, ProjectProjectOrigin)
            R.id.project_project_destination_parent -> startActivityForResult(ProjectProjectDestination, ProjectProjectDestination)
            R.id.project_project_submit_button   -> openTransactions()
        }
    }

    private fun openTransactions() {
        viewModel.addItemToDatabase(OriginDestinationEntity(CategoryProjectProject, bundle.getInt("originId"), bundle.getString("originName"),
                                                            bundle.getInt("destinationId"), bundle.getString("destinationName")))
        bundle.putInt("category_type", CategoryProjectProject)
        navController.navigate(R.id.action_nav_project_to_project_to_scannedItemsFragment, bundle)
    }

    private fun startActivityForResult(requestCode: Int, origin: Int) {
        startActivityForResult(Intent(context, ProjectsLocationsListActivity::class.java).putExtra("origin", origin), requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activity?.overridePendingTransition(R.anim.slide_in_left, 0);

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ProjectProjectOrigin      -> setOriginInfo(ProjectProjectOrigin, data)
                ProjectProjectDestination -> setDestinationInfo(ProjectProjectDestination, data)
            }
        }

        if (!binding.projectProjectOriginText.text.isNullOrEmpty()) { // && !binding.pickingDestinationText.text.isNullOrEmpty()) {
            binding.projectProjectSubmitButton.isEnabled = true
            binding.projectProjectSubmitButton.backgroundTintList = ContextCompat.getColorStateList(context!!, R.color.colorPrimary)
        }
    }

    private fun setOriginInfo(requestCode: Int, data: Intent?) {
        binding.projectProjectOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
        bundle.putInt("originId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("originName", data.extras!!.getString("projectOrLocationName"))
        binding.projectProjectOriginText.text = data.extras!!.getString("projectOrLocationName")
    }

    private fun setDestinationInfo(requestCode: Int, data: Intent?) {
        binding.projectProjectDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
        bundle.putInt("destinationId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("destinationName", data.extras!!.getString("projectOrLocationName"))
        binding.projectProjectDestinationText.text = data.extras!!.getString("projectOrLocationName")
    }
}