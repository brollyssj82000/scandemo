package com.ionScanApp.ui

import android.app.AlertDialog
import android.content.Context
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView
import com.ionScanApp.R

open class BaseFragment : Fragment() {

    private var errorDialog: AlertDialog? = null
    private var errorDialogBuilder: AlertDialog.Builder? = null

    fun showCustomErrorDialog(context: Context?, message: String) {
        if (isRemoving){
            errorDialog?.dismiss()
            return
        }

        errorDialog?.dismiss()

        errorDialogBuilder = AlertDialog.Builder(context)

        val dialogView = layoutInflater.inflate(R.layout.fragment_error_dialog, null)

        val cancelButton = dialogView.findViewById<View>(R.id.error_back_to_menu_button) as MaterialButton
        val messageTextView = dialogView.findViewById<View>(R.id.error_text) as MaterialTextView
        messageTextView.text = message
        cancelButton.setOnClickListener {
            errorDialog?.dismiss()
        }
        errorDialogBuilder?.setView(dialogView)
        errorDialog = errorDialogBuilder?.create()
        errorDialog?.show()
    }

    fun showErrorDialog(context: Context?, title: String, message: String) {
        if (isRemoving) return
        errorDialog?.dismiss()
        errorDialog = AlertDialog.Builder(context)
            .setTitle(title)
            .setIcon(R.drawable.ic_warning)
            .setMessage(message)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .create()
        errorDialog?.show()
    }

    override fun onDestroy() {
        errorDialog?.dismiss()
        super.onDestroy()
    }

    open fun onBackPressedHandle(navController: NavController) {
        requireActivity().onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                errorDialog?.dismiss()
                // navController.navigate(R.id.action_nav_stock_to_project_to_nav_home)
                navController.popBackStack(R.id.nav_home, false)
            }
        })
    }
}