package com.ionScanApp.ui.picking

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.OriginDestinationEntity
import com.ionScanApp.databinding.FragmentPickingBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.common.CategoryProjectStockPicking
import com.ionScanApp.ui.common.DeleteDatabaseProjecstLocationSelectionCache
import com.ionScanApp.ui.common.OriginDestinationViewModel
import com.ionScanApp.ui.common.ProjectStockDestination
import com.ionScanApp.ui.common.ProjectStockOrigin
import com.ionScanApp.ui.common.ProjectsLocationsListActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class PickingFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentPickingBinding
    private lateinit var navController: NavController
    private var bundle = Bundle()
    private val viewModel: OriginDestinationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.clearDatabaseProjecstLocationSelectionCache(DeleteDatabaseProjecstLocationSelectionCache)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPickingBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        binding.pickingSubmitButton.isEnabled = false

        viewModel.getSelectedOriginDestinationFromDb(CategoryProjectStockPicking)

        setOnClickListeners()
        setupObservers()
        onBackPressedHandle(navController)
    }

    private fun setupObservers() {
        viewModel.liveDataInsertedItems.observe(viewLifecycleOwner, Observer {

        })

        viewModel.liveDataOriginDestination.observe(viewLifecycleOwner, Observer {
            binding.pickingOriginText.text = it.originName
            binding.pickingDestinationText.text = it.destinationName
            bundle.putInt("originId", it.originId!!)

            if (it.destinationName.isNullOrBlank()) {
                binding.pickingDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom_white)
            } else {
                bundle.putInt("destinationId", it.destinationId!!)
                binding.pickingDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
            }

            if (!it.originName.isNullOrBlank()) {
                binding.pickingOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
                binding.pickingSubmitButton.isEnabled = true
                binding.pickingSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
            }
        })
    }

    private fun setOnClickListeners() {
        binding.pickingOriginParent.setOnClickListener(this)
        binding.pickingDestinationParent.setOnClickListener(this)
        binding.pickingSubmitButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.picking_origin_parent  -> startActivityForResult(ProjectStockOrigin, ProjectStockOrigin)
            R.id.picking_destination_parent -> startActivityForResult(ProjectStockDestination, ProjectStockDestination)
            R.id.picking_submit_button   -> openTransactions()
        }
    }

    private fun openTransactions() {
        viewModel.addItemToDatabase(OriginDestinationEntity(CategoryProjectStockPicking, bundle.getInt("originId"), bundle.getString("originName"),
                                                                                  bundle.getInt("destinationId"), bundle.getString("destinationName")))
        bundle.putInt("category_type", CategoryProjectStockPicking)
        navController.navigate(R.id.action_nav_picking_to_scannedItemsFragment, bundle)
    }

    private fun startActivityForResult(requestCode: Int, origin: Int) {
        startActivityForResult(Intent(context, ProjectsLocationsListActivity::class.java).putExtra("origin", origin), requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activity?.overridePendingTransition(R.anim.slide_in_left, 0);

        if (resultCode == RESULT_OK) {
            when (requestCode) {
                ProjectStockOrigin      -> setOriginInfo(ProjectStockOrigin, data)
                ProjectStockDestination -> setDestinationInfo(ProjectStockDestination, data)
            }
        }

        if (!binding.pickingOriginText.text.isNullOrEmpty()) { // && !binding.pickingDestinationText.text.isNullOrEmpty()) {
            binding.pickingSubmitButton.isEnabled = true
            binding.pickingSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
        }
    }

    private fun setOriginInfo(requestCode: Int, data: Intent?) {
        binding.pickingOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
        bundle.putInt("originId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("originName", data.extras!!.getString("projectOrLocationName"))
        binding.pickingOriginText.text = data.extras!!.getString("projectOrLocationName")
    }

    private fun setDestinationInfo(requestCode: Int, data: Intent?) {
        binding.pickingDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
        bundle.putInt("destinationId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("destinationName", data.extras!!.getString("projectOrLocationName"))
        binding.pickingDestinationText.text = data.extras!!.getString("projectOrLocationName")
    }
}