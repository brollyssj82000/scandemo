package com.ionScanApp.ui

import android.app.AlertDialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.ionScanApp.R


open class BaseActivity : AppCompatActivity() {

    private var errorDialog: AlertDialog? = null

    fun showErrorDialog(context: Context, title: String, message: String) {
        if (isFinishing) {
            errorDialog?.dismiss()
            return
        }

        errorDialog?.dismiss()

        errorDialog = AlertDialog.Builder(context)
            .setTitle(title)
            .setIcon(R.drawable.ic_warning)
            .setMessage(message)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .create()

        errorDialog?.show()
    }

    override fun onDestroy() {
        errorDialog?.dismiss()
        super.onDestroy()
    }
}