package com.ionScanApp.ui.login

import android.content.Context
import android.content.Intent
import android.graphics.*
import androidx.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import androidx.annotation.StringRes
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import com.ionScanApp.MainActivity
import com.ionScanApp.R
import com.ionScanApp.databinding.ActivityLoginBinding
import com.ionScanApp.ui.BaseActivity
import com.ionScanApp.ui.common.utils.setColorFilter
import com.ionScanApp.ui.common.utils.visible
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.ionScanApp.ui.common.utils.error

class LoginActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent = Intent(context, LoginActivity::class.java)
    }

    val viewModel: LoginViewModel by viewModel()

    lateinit var binding: ActivityLoginBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setupNav()
        setupObservers()
        edit_password.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                button_sign_in.performClick()
            }
            false
        }
    }

    private fun setupNav() {
        viewModel.navigateToMain.observe(this, Observer {
            startActivity(MainActivity.newIntent(this))
            finish()
        })
    }

    private fun setupObservers() {

        viewModel.urlError.observe(this, Observer {
            text_layout_url.error(getStringOrNull(it))
        })

        viewModel.usernameError.observe(this, Observer {
            text_layout_username.error(getStringOrNull(it))
        })

        viewModel.passwordError.observe(this, Observer {
            text_layout_password.error(getStringOrNull(it))
        })

        viewModel.progressVisible.observe(this, Observer {
            progress_login.visible = it
        })

        viewModel.canSubmit.observe(this, Observer {
            button_sign_in.isEnabled = it ?: false
        })

        viewModel.intError.observe(this, Observer {
            showErrorDialog(this, getString(R.string.log_in), getString(it))
            progress_login.visible = false
        })

        viewModel.stringError.observe(this, Observer {
            showErrorDialog(this, getString(R.string.log_in), it)
            progress_login.visible = false
        })
    }

    private fun getStringOrNull(@StringRes res: Int?): String? {
        return if (res == null) {
            null
        } else {
            getString(res)
        }
    }

}