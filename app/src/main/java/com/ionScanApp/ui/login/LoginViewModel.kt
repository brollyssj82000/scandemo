package com.ionScanApp.ui.login

import android.view.View
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.ionScanApp.R
import com.ionScanApp.data.remote.repository.LoginRepository
import com.ionScanApp.data.local.entity.LoginEntity
import com.ionScanApp.data.remote.api.response.convertToUserEntity
import com.ionScanApp.ui.BaseViewModel
import com.ionScanApp.ui.common.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginViewModel(private val loginRepository: LoginRepository) : BaseViewModel() {

    val url = MutableLiveData<String>()
    val urlError = MediatorLiveData<Int>().also { result ->
        result.addSource(url) { result.value = urlError() }
    }
    val username = MutableLiveData<String>()
    val usernameError = MediatorLiveData<Int>().also { result ->
        result.addSource(username) { result.value = usernameError() }
    }
    val password = MutableLiveData<String>()
    val passwordError = MediatorLiveData<Int>().also { result ->
        result.addSource(password) { result.value = passwordError() }
    }

    val progressVisible = MutableLiveData<Boolean>()
    val navigateToMain = MutableLiveData<Boolean>()

    val formValid = MediatorLiveData<Boolean>().also { result ->
        result.addSource(url)       { result.value = validateForm() }
        result.addSource(username)  { result.value = validateForm() }
        result.addSource(password)  { result.value = validateForm() }
    }

    val canSubmit = MediatorLiveData<Boolean>().also { result ->
        result.addSource(formValid) { result.value = it }
        result.addSource(progressVisible) { result.value = if (it != null) !it else false }
    }

    init {
        getLoginData()
    }

    private fun getLoginData() = launch {
        loginRepository.getLoginMaybe()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { handleLoginEntity(it) },
                { err -> })
    }

    private fun handleLoginEntity(loginEntity: LoginEntity) {
        url.value       = loginEntity.url
        username.value  = loginEntity.username
        password.value  = loginEntity.password
    }

    fun onLoginClick(view: View) {
        if (validateForm()) {
            val loginEntity = LoginEntity(0, username.value!!, password.value!!, url.value!!, false)
            performLogin(loginEntity)
        }
    }

    private fun performLogin(loginEntity: LoginEntity) = launch {
        loginRepository.performLogin()
            .doOnSubscribe {
                progressVisible.postValue(true)
                loginRepository.deleteLogin()
                loginRepository.insertLogin(loginEntity)
            }
            .subscribeOn(Schedulers.io())
            .doOnSuccess {
                val userEntity = convertToUserEntity(it)
                loginRepository.insertUser(userEntity)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { navigateToMain.value = true },
                { ErrorHandler.handle(it, intError, stringError) })
    }

    private fun validateForm() = urlError() == null && usernameError() == null && passwordError() == null

    private fun urlError(): Int? = validate(url.value, listOf(urlRequired, urlValid))

    private fun usernameError(): Int? = validate(username.value, listOf(usernameRequired))

    private fun passwordError(): Int? = validate(password.value, listOf(passwordRequired))

}


val urlRequired = ValidatorPair(::isRequired, R.string.error_url_required)
val urlValid    = ValidatorPair(::isValidUrl, R.string.error_invalid_URL)
val usernameRequired = ValidatorPair(::isRequired, R.string.error_username_required)
val passwordRequired = ValidatorPair(::isRequired, R.string.error_password_required)