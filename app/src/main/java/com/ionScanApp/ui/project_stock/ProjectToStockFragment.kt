package com.ionScanApp.ui.project_stock

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.OriginDestinationEntity
import com.ionScanApp.databinding.FragmentProjectStockBinding
import com.ionScanApp.ui.BaseFragment
import com.ionScanApp.ui.common.CategoryProjectStock
import com.ionScanApp.ui.common.DeleteDatabaseProjecstLocationSelectionCache
import com.ionScanApp.ui.common.OriginDestinationViewModel
import com.ionScanApp.ui.common.ProjectStockDestination
import com.ionScanApp.ui.common.ProjectStockOrigin
import com.ionScanApp.ui.common.ProjectsLocationsListActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ProjectToStockFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentProjectStockBinding
    private lateinit var navController: NavController
    private var bundle = Bundle()
    private val viewModel: OriginDestinationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.clearDatabaseProjecstLocationSelectionCache(DeleteDatabaseProjecstLocationSelectionCache)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentProjectStockBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        binding.projectStockSubmitButton.isEnabled = false

        viewModel.getSelectedOriginDestinationFromDb(CategoryProjectStock)

        setOnClickListeners()
        setupObservers()
        onBackPressedHandle(navController)
    }

    private fun setupObservers() {
        viewModel.liveDataInsertedItems.observe(viewLifecycleOwner, Observer {

        })

        viewModel.liveDataOriginDestination.observe(viewLifecycleOwner, Observer {
            binding.projectStockOriginText.text = it.originName
            binding.projectStockDestinationText.text = it.destinationName
            bundle.putInt("originId", it.originId!!)

            if (it.destinationName.isNullOrBlank()) {
                binding.projectStockDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom_white)
            } else {
                bundle.putInt("destinationId", it.destinationId!!)
                binding.projectStockDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
            }

            if (!it.originName.isNullOrBlank()) {
                binding.projectStockOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
                binding.projectStockSubmitButton.isEnabled = true
                binding.projectStockSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
            }
        })
    }

    private fun setOnClickListeners() {
        binding.projectStockOriginParent.setOnClickListener(this)
        binding.projectStockDestinationParent.setOnClickListener(this)
        binding.projectStockSubmitButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.project_stock_origin_parent  -> startActivityForResult(ProjectStockOrigin, ProjectStockOrigin)
            R.id.project_stock_destination_parent -> startActivityForResult(ProjectStockDestination, ProjectStockDestination)
            R.id.project_stock_submit_button   -> openTransactions()
        }
    }

    private fun openTransactions() {
        viewModel.addItemToDatabase(OriginDestinationEntity(CategoryProjectStock, bundle.getInt("originId"), bundle.getString("originName"),
                                                            bundle.getInt("destinationId"), bundle.getString("destinationName")))
        bundle.putInt("category_type", CategoryProjectStock)
        navController.navigate(R.id.action_nav_project_to_stock_to_scannedItemsFragment, bundle)
    }

    private fun startActivityForResult(requestCode: Int, origin: Int) {
        startActivityForResult(Intent(context, ProjectsLocationsListActivity::class.java).putExtra("origin", origin), requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        activity?.overridePendingTransition(R.anim.slide_in_left, 0);

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ProjectStockOrigin      -> setOriginInfo(ProjectStockOrigin, data)
                ProjectStockDestination -> setDestinationInfo(ProjectStockDestination, data)
            }
        }

        if (!binding.projectStockOriginText.text.isNullOrEmpty()) { // && !binding.pickingDestinationText.text.isNullOrEmpty()) {
            binding.projectStockSubmitButton.isEnabled = true
            binding.projectStockSubmitButton.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
        }
    }

    private fun setOriginInfo(requestCode: Int, data: Intent?) {
        binding.projectStockOriginParent.setBackgroundResource(R.drawable.rounded_corners_top)
        bundle.putInt("originId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("originName", data.extras!!.getString("projectOrLocationName"))
        binding.projectStockOriginText.text = data.extras!!.getString("projectOrLocationName")
    }

    private fun setDestinationInfo(requestCode: Int, data: Intent?) {
        binding.projectStockDestinationParent.setBackgroundResource(R.drawable.rounded_corners_bottom)
        bundle.putInt("destinationId", data?.extras!!.getInt("projectOrLocationId"))
        bundle.putString("destinationName", data.extras!!.getString("projectOrLocationName"))
        binding.projectStockDestinationText.text = data.extras!!.getString("projectOrLocationName")
    }
}