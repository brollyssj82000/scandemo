package com.ionScanApp.ui.common.utils

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable

import timber.log.Timber
import android.R.color
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.os.Build




fun getColor(color: String?, defaultColor: Int): Int {
    var result = defaultColor
    try {
        result = Color.parseColor(color)
    } catch (e: Throwable) {
        if (color != null) {
            Timber.w("Tried to parse as invalid color: $color")
        }
    }
    return result
}


fun Drawable.setColorFilter(color: Int) {
    mutate()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
    } else {
        setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    }
}
