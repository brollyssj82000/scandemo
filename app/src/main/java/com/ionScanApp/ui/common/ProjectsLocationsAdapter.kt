package com.ionScanApp.ui.common

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.ProjectsLocationsResponse
import com.ionScanApp.ui.common.utils.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.project_location_list_item.*

class ProjectsLocationsAdapter(val itemClickListener: ItemClickListener<ProjectsLocationsResponse>) : PagedListAdapter<ProjectsLocationsResponse, ProjectsLocationsAdapter.ProjectsLocationsViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ProjectsLocationsResponse>() {
            override fun areItemsTheSame(oldItem: ProjectsLocationsResponse, newItem: ProjectsLocationsResponse): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ProjectsLocationsResponse, newItem: ProjectsLocationsResponse): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectsLocationsViewHolder = ProjectsLocationsViewHolder(parent.inflate(R.layout.project_location_list_item))


    override fun onBindViewHolder(holder: ProjectsLocationsViewHolder, position: Int) {
        if (getItem(position) != null) {
            holder.bind(getItem(position)!!)
        } else {
            holder.clear()
        }
    }

    inner class ProjectsLocationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        fun clear() {
            projectLocationName.text = ""
        }

        override val containerView: View?
            get() = itemView

        fun bind(item: ProjectsLocationsResponse) {
            projectLocationName.text = item.name

            projectLocationName.setOnClickListener {
                itemClickListener.onItemClick(adapterPosition, 0, item)
            }
        }
    }
}