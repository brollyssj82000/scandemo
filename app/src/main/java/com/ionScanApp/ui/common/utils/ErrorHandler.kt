package com.ionScanApp.ui.common.utils

import androidx.lifecycle.MutableLiveData
import com.ionScanApp.data.remote.api.error.RetrofitException
import com.ionScanApp.R
import timber.log.Timber


object ErrorHandler {

    fun handle(err: Throwable, intError: MutableLiveData<Int>, stringError: MutableLiveData<String>) {
        if (hasStringError(err)) {
            stringError.postValue(getStringErrorMessage(err))
        } else {
            intError.postValue(getIntErrorMessage(err))
        }

    }

    private fun hasStringError(err: Throwable): Boolean {
        if (err is RetrofitException
                && err.getKind() == RetrofitException.Kind.SERVER) {
            return true
        }
        return false
    }

    private fun getStringErrorMessage(err: Throwable): String {
        err as RetrofitException
        return err.getErrorMessage() ?: "Error message not specified"
    }

    private fun getIntErrorMessage(err: Throwable): Int {
        return when (err) {
            is RetrofitException -> handleRetrofitException(err)
            else -> handleNonRetrofitError(err)
        }
    }

    private fun handleRetrofitException(err: RetrofitException): Int {
        return when (err.getKind()) {
            RetrofitException.Kind.HTTP         -> handleHttpError(err)
            RetrofitException.Kind.SERVER       -> handleServerError(err)
            RetrofitException.Kind.SERVER_INIT  -> handleServerInitError(err)
            RetrofitException.Kind.NETWORK      -> handleNetworkError(err)
            RetrofitException.Kind.CONNECTIVITY -> handleConnectivityError()
            RetrofitException.Kind.TIMEOUT      -> handleTimeoutError()
            RetrofitException.Kind.UNEXPECTED   -> handleUnexpectedError(err)
        }
    }

    private fun handleHttpError(err: RetrofitException): Int {
        Timber.e(err, "HTTP ERROR")
        return when (err.getCode()) {
            401 -> R.string.error_sign_in_credentials
            else -> R.string.error_unhandled_http
        }
    }

    private fun handleServerError(err: RetrofitException): Int {
        Timber.e(err, "SERVER ERROR")
        return R.string.error_server
    }

    private fun handleServerInitError(err: RetrofitException): Int {
        Timber.e(err, "SERVER INIT ERROR")
        return R.string.error_server_init
    }

    private fun handleNetworkError(err: RetrofitException): Int {
        Timber.e(err, "NETWORK ERROR")
        return R.string.error_sign_in_url
    }

    private fun handleConnectivityError(): Int {
        Timber.d("CONNECTIVITY ERROR")
        return R.string.error_no_internet
    }

    private fun handleTimeoutError(): Int {
        Timber.d("TIMEOUT ERROR")
        return R.string.error_connection_timeout
    }

    private fun handleUnexpectedError(err: RetrofitException): Int {
        Timber.e(err, "UNEXPECTED ERROR")
        return R.string.error_unknown
    }

    private fun handleNonRetrofitError(err: Throwable): Int {
        Timber.e(err, "OTHER ERROR")
        return R.string.error_exception
    }
}
