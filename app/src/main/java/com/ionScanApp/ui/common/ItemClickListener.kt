package com.ionScanApp.ui.common

interface ItemClickListener<T> {

    fun onItemClick(adapterPosition: Int, id: Int, item: T)
}