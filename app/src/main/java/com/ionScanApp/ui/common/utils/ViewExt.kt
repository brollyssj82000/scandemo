package com.ionScanApp.ui.common.utils

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import com.google.android.material.textfield.TextInputLayout
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

var View.visible: Boolean
    get() {
        return this.visibility == View.VISIBLE
    }
    set(visible) {
        this.visibility = if (visible) View.VISIBLE else View.GONE
    }

var View.invisible: Boolean
    get() {
        return this.visibility == View.INVISIBLE
    }
    set(visible) {
        this.visibility = if (invisible) View.INVISIBLE else View.VISIBLE
    }

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

fun TextInputLayout.error(error: CharSequence?) {
    isErrorEnabled = error != null
    setError(error)
}