package com.ionScanApp.ui.common

interface EditTextChangeListener {

    fun onToBookEditTextChange(toBookValue: Int?, itemId: Int, adapterPosition: Int)
    fun onToBookEditTextChangeError(toBookValue: Int?)
    fun onToBookEditTextChangeEmptyError(itemName: String?, adapterPosition: Int)
}