package com.ionScanApp.ui.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ionScanApp.data.local.dao.ProjectsLocationsDao
import com.ionScanApp.data.local.entity.ProjectsLocationsResponse
import com.ionScanApp.data.remote.repository.ProjectsLocationsRepository
import com.ionScanApp.ui.BaseViewModel
import com.ionScanApp.ui.common.utils.ErrorHandler
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class ProjectsLocationsViewModel(private val projectsLocationsDao: ProjectsLocationsDao, private val projectsLocationsRepository: ProjectsLocationsRepository) : BaseViewModel() {

    var liveDataProjectsLocations = MutableLiveData<List<ProjectsLocationsResponse>>()

    val itemPagedList: LiveData<PagedList<ProjectsLocationsResponse>> by lazy {
        val dataSourceFactory = projectsLocationsDao.getProjectsStockLocations()
        val config = PagedList.Config.Builder()
            .setPageSize(60)
            .setInitialLoadSizeHint(20)
            .setPrefetchDistance(40)
            .setEnablePlaceholders(false)
            .build()
        LivePagedListBuilder(dataSourceFactory, config).build()
    }

    fun clearDatabaseProjecstLocations(source: Boolean) = clearDatabase(source) {
        projectsLocationsDao.deleteDatabaseEntries()
    }

    fun getProjectsFromApi(page: Int, pageSize: Int, query: String) = launch {
        projectsLocationsRepository.getProjectsApi(0, page, pageSize, query)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onNext = {
                addItemsToDatabase(it)
                liveDataProjectsLocations.value = it
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun getLocationsFromApi(page: Int, pageSize: Int, query: String) = launch {
        projectsLocationsRepository.getStockLocationsApi(page, pageSize, query)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onNext = {
                addItemsToDatabase(it)
                liveDataProjectsLocations.value = it
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    private fun addItemsToDatabase(items: List<ProjectsLocationsResponse>) = launch {
        Completable.fromAction { projectsLocationsDao.insertItems(items) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {
                //getDdSize()
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }
}