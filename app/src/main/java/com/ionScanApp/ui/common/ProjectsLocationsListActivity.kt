package com.ionScanApp.ui.common

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ionScanApp.R
import com.ionScanApp.data.local.entity.ProjectsLocationsResponse
import com.ionScanApp.databinding.ActivitySelectProjectLocationListBinding
import com.ionScanApp.ui.BaseActivity
import com.ionScanApp.ui.common.utils.visible
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_select_project_location_list_include.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit


class ProjectsLocationsListActivity : BaseActivity(), ItemClickListener<ProjectsLocationsResponse> {

    private lateinit var binding: ActivitySelectProjectLocationListBinding
    private val disposables = CompositeDisposable()
    private var origin: Int? = null
    private var isLoadingMore = false
    private var hasLoadedAllItems = false

    private var FIRSTPAGE = 1
    private lateinit var paginationListener: PaginationScrollListener

    private var projectsListAdapter: PagedListAdapter<ProjectsLocationsResponse, ProjectsLocationsAdapter.ProjectsLocationsViewHolder> = ProjectsLocationsAdapter(this)

    private val viewModel: ProjectsLocationsViewModel by viewModel()

    override fun startActivity(intent: Intent?) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectProjectLocationListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarList)

        origin = intent.extras?.get("origin") as Int

        setupCategoryTypeTitles(origin)

        getProjectsAndLocations(origin)

        setupRecycler()
        setupObservers()

        binding.activityListIncludeId.swipeRefreshList.setOnRefreshListener {
            binding.activityListIncludeId.swipeRefreshList.isRefreshing = true
            viewModel.clearDatabaseProjecstLocations(DeleteDatabaseProjecstLocations)
            FIRSTPAGE = 1
        }
    }

    private fun setupCategoryTypeTitles(origin: Int?) {
        when (origin) {
            ProjectStockOrigin, StockProjectDestination -> title = "Select project"
            ProjectStockDestination, StockProjectOrigin -> title = "Select location"
            ProjectProjectOrigin      -> title = "Select source project"
            ProjectProjectDestination -> title = "Select destinatination project"
            StockStockOrigin          -> title = "Select source location"
            StockStockDestination     -> title = "Select destination location"
        }
    }

    private fun getProjectsAndLocations(origin: Int?) {
        when (origin) {
            ProjectStockOrigin, ProjectProjectOrigin, ProjectProjectDestination, StockProjectDestination -> viewModel.getProjectsFromApi(1, 50, "")
            ProjectStockDestination, StockStockOrigin, StockStockDestination, StockProjectOrigin         -> viewModel.getLocationsFromApi(1, 50, "")
        }
    }

    private fun setupObservers() {

        viewModel.itemPagedList.observe(this, Observer {
            projectsListAdapter.submitList(it)

            binding.activityListIncludeId.swipeRefreshList.isRefreshing = false
        })

        viewModel.liveDataProjectsLocations.observe(this, Observer {
            when {
                it.isNullOrEmpty() -> {
                    viewModel.clearDatabaseProjecstLocations(DeleteDatabaseProjecstLocations)
                    unbindPagination()
                    binding.root.projects_locations_no_data.visibility = View.VISIBLE
                }

                it.size in 1..49   -> {
                    unbindPagination()
                }

                else               -> {
                    hasLoadedAllItems = false
                    isLoadingMore = true
                }
            }
        })

        viewModel.liveDataClearDatabaseProjectsLocations.observe(this, Observer {
            getProjectsAndLocations(origin)
        })

        viewModel.showProgress.observe(this, Observer {
            binding.activityListIncludeId.progressBarList.visible = it!!
        })

        viewModel.stringError.observe(this, Observer {
            showErrorDialog(this, "Error", it)
        })
    }

    private fun unbindPagination() {
        hasLoadedAllItems = true
        isLoadingMore = false
        binding.activityListIncludeId.projectsLocationsList.removeOnScrollListener(paginationListener)
    }

    private fun setupRecycler() {
        binding.activityListIncludeId.projectsLocationsList.apply {
            layoutManager = LinearLayoutManager(this@ProjectsLocationsListActivity)
            addItemDecoration(DividerItemDecoration(this.context, (binding.activityListIncludeId.projectsLocationsList.layoutManager as LinearLayoutManager).orientation))
            setHasFixedSize(true)
            itemAnimator = DefaultItemAnimator()
            //    setItemViewCacheSize(50)
            if (!projectsListAdapter.hasObservers()) {
                adapter?.setHasStableIds(true)
            }
            adapter = projectsListAdapter
        }
        setupRecylerPagination()
    }

    private fun setupRecylerPagination() {
        paginationListener = object : PaginationScrollListener(binding.activityListIncludeId.projectsLocationsList.layoutManager as LinearLayoutManager, 50) {
            override fun loadMoreItems() {
                isLoadingMore = true
                when (origin) {
                    ProjectProjectOrigin      -> viewModel.getProjectsFromApi(++FIRSTPAGE, 50, "")
                    ProjectProjectDestination -> viewModel.getLocationsFromApi(++FIRSTPAGE, 50, "")
                }
            }

            override fun isLastPage(): Boolean {
                return hasLoadedAllItems
            }

            override fun isLoading(): Boolean {
                return isLoadingMore
            }
        }
        binding.activityListIncludeId.projectsLocationsList.addOnScrollListener(paginationListener)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_projects_locations_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val mSearchView = menu.findItem(R.id.action_search).actionView as SearchView
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        mSearchView.maxWidth = resources.displayMetrics.widthPixels

        when (origin) {

            ProjectStockOrigin, ProjectProjectOrigin, ProjectProjectDestination, StockProjectDestination -> disposables.add(RxSearchView.queryTextChanges(mSearchView).map<String> {
                    charSequence: CharSequence? -> charSequence?.toString()?.trim { it <= ' ' }}.throttleLast(100, TimeUnit.MILLISECONDS).debounce(100, TimeUnit.MILLISECONDS)
                                                                                                .observeOn(AndroidSchedulers.mainThread()).subscribe { searchText: String? ->
                                                                                                                                                       viewModel.clearDatabaseProjecstLocations(DeleteDatabaseProjecstLocations)
                                                                                                                                                       viewModel.getProjectsFromApi(0, 50, searchText!!)
                })

            ProjectStockDestination, StockStockOrigin, StockStockDestination, StockProjectOrigin         -> disposables.add(RxSearchView.queryTextChanges(mSearchView).map<String> {
                    charSequence: CharSequence? -> charSequence?.toString()?.trim { it <= ' ' }}.throttleLast(100, TimeUnit.MILLISECONDS).debounce(100, TimeUnit.MILLISECONDS)
                                                                                                .observeOn(AndroidSchedulers.mainThread()).subscribe { searchText: String? ->
                                                                                                                                                       viewModel.clearDatabaseProjecstLocations(DeleteDatabaseProjecstLocations)
                                                                                                                                                       viewModel.getLocationsFromApi(0, 50, searchText!!)
                })
        }
        return true
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    override fun onItemClick(adapterPosition: Int, itemId: Int, item: ProjectsLocationsResponse) {
        val resultIntent = Intent()
        resultIntent.putExtra("projectOrLocationId", item.id!!)
        resultIntent.putExtra("projectOrLocationName", item.name!!)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }
}
