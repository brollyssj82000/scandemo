package com.ionScanApp.ui.common.utils

import android.util.Patterns


typealias ValidatorPair = Pair<(String?) -> Boolean, Int>

fun validate(value: String?, validators: List<ValidatorPair>): Int? {
    var errorMessage: Int? = null
    for (validator in validators) {
        if (!validator.first(value)) {
            errorMessage = validator.second
            break
        }
    }
    return errorMessage
}

fun isRequired(value: String?) = !value.isNullOrBlank()

fun isValidUrl(value: String?) = Patterns.WEB_URL.matcher(value).matches()