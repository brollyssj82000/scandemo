package com.ionScanApp.ui.common

const val Project = 0
const val Stock = 1
const val Purchase = 2
const val Customer = 3
const val ProductionProject = 4

const val CategoryProjectStockPicking = -1
const val CategoryProjectStock = 0
const val CategoryProjectProject = 1
const val CategoryStockStock = 2
const val CategoryStockProject = 3

const val ProjectStockOrigin = 0
const val ProjectStockDestination = 1

const val ProjectProjectOrigin = 2
const val ProjectProjectDestination = 3

const val StockStockOrigin = 4
const val StockStockDestination = 5

const val StockProjectOrigin = 6
const val StockProjectDestination = 7

const val DeleteDatabaseSerialNumbersSource = true
const val DeleteDatabaseTransactionsSource = true
const val DeleteDatabaseProjecstLocations = true
const val DeleteDatabaseProjecstLocationSelectionCache = true
