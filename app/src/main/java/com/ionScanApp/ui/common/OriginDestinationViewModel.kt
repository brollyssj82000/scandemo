package com.ionScanApp.ui.common

import androidx.lifecycle.MutableLiveData
import com.ionScanApp.data.local.dao.OriginDestinationDao
import com.ionScanApp.data.local.entity.OriginDestinationEntity
import com.ionScanApp.ui.BaseViewModel
import com.ionScanApp.ui.common.utils.ErrorHandler
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class OriginDestinationViewModel(private val originDestinationDao: OriginDestinationDao) : BaseViewModel() {

    var liveDataOriginDestination = MutableLiveData<OriginDestinationEntity>()
    var liveDataInsertedItems = MutableLiveData<Boolean>()

    fun clearDatabaseProjecstLocationSelectionCache(source: Boolean) = clearDatabase(source) {
        Completable.fromAction { originDestinationDao.deleteDatabaseEntries() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {

            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun getSelectedOriginDestinationFromDb(category_type: Int) = launch {
        originDestinationDao.getSelectedOriginDestinationByCategory(category_type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onSuccess = {
                liveDataOriginDestination.value = it
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

    fun addItemToDatabase(item: OriginDestinationEntity) = launch {
        Completable.fromAction { originDestinationDao.insertItem(item) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .doOnSubscribe { showProgress() }
            .doFinally {
                hideProgress()
            }
            .subscribeBy(onComplete = {
                liveDataInsertedItems.value = true
            }, onError = {
                ErrorHandler.handle(it, intError, stringError)
            })
    }

}
