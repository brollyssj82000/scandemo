package com.ionScanApp.ui.common.utils

import androidx.drawerlayout.widget.DrawerLayout


inline fun consume(action: () -> Unit): Boolean {
    action()
    return true
}

inline fun DrawerLayout.consume(action: () -> Unit): Boolean {
    action()
    closeDrawers()
    return true
}
