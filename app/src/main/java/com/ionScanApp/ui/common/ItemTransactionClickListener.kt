package com.ionScanApp.ui.common

interface ItemTransactionClickListener {
    fun onItemClickOpenItemTransaction(id: Int)
}