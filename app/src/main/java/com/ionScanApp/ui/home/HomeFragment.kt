package com.ionScanApp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ionScanApp.R
import com.ionScanApp.databinding.FragmentHomeBinding
import com.ionScanApp.ui.BaseFragment

class HomeFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        setOnClickListeners()
    }

    private fun setOnClickListeners() {
        binding.stockProjectTitleParent.setOnClickListener(this)
        binding.stockStockTitleParent.setOnClickListener(this)
        binding.projectStockTitleParent.setOnClickListener(this)
        binding.projectProjectTitleParent.setOnClickListener(this)
        binding.pickingTitleParent.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.stock_project_title_parent   -> navController.navigate(R.id.nav_stock_to_project)
            R.id.stock_stock_title_parent     -> navController.navigate(R.id.nav_stock_to_stock)
            R.id.project_stock_title_parent   -> navController.navigate(R.id.nav_project_to_stock)
            R.id.project_project_title_parent -> navController.navigate(R.id.nav_project_to_project)
            R.id.picking_title_parent         -> navController.navigate(R.id.nav_picking)
        }
    }
}